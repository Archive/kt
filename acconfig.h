/* acconfig.h
   This file is in the public domain.

   Descriptive text for the C preprocessor macros that
   the distributed Autoconf macros can define.
   No software package will use all of them; autoheader copies the ones
   your configure.in uses into your configuration header file templates.

   The entries are in sort -df order: alphabetical, case insensitive,
   ignoring punctuation (such as underscores).  Although this order
   can split up related entries, it makes it easier to check whether
   a given entry is in the file.

   Leave the following blank line there!!  Autoheader needs it.  */


#undef KT_MAJOR_VERSION
#undef KT_MINOR_VERSION
#undef KT_MICRO_VERSION
#undef KT_VERSION

#undef HAVE_DIRENT_H
#undef HAVE_DOPRNT
#undef HAVE_IPC_H
#undef HAVE_NDIR_H
#undef HAVE_SHM_H
#undef HAVE_SYS_DIR_H
#undef HAVE_SYS_NDIR_H
#undef HAVE_SYS_SELECT_H
#undef HAVE_SYS_TIME_H
#undef HAVE_UNISTD_H
#undef HAVE_VPRINTF

#undef NO_DIFFTIME
#undef NO_FD_SET

#undef RAND_FUNC
#undef SRAND_FUNC

#undef PACKAGE
#undef VERSION


/* Leave that blank line there!!  Autoheader needs it.
   If you're adding to this file, keep in mind:
   The entries are in sort -df order: alphabetical, case insensitive,
   ignoring punctuation (such as underscores).  */
