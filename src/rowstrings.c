/*

Keg Tracker.  A GTK-based mod-editor a la fast-tracker.
Copyright (C) 1998 Pete Ryland

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author can be contacted at pdr@pdr.ml.org

*/

static char *_id = "$Id$";

char note_strings[256][4] = {
  "---", /*"C-0",*/ "C#0", "D-0", "Eb0", "E-0", "F-0", "F#0", "G-0", "Ab0", "A-0", "Bb0", "B-0",
  "C-1", "C#1", "D-1", "Eb1", "E-1", "F-1", "F#1", "G-1", "Ab1", "A-1", "Bb1", "B-1",
  "C-2", "C#2", "D-2", "Eb2", "E-2", "F-2", "F#2", "G-2", "Ab2", "A-2", "Bb2", "B-2",
  "C-3", "C#3", "D-3", "Eb3", "E-3", "F-3", "F#3", "G-3", "Ab3", "A-3", "Bb3", "B-3",
  "C-4", "C#4", "D-4", "Eb4", "E-4", "F-4", "F#4", "G-4", "Ab4", "A-4", "Bb4", "B-4",
  "C-5", "C#5", "D-5", "Eb5", "E-5", "F-5", "F#5", "G-5", "Ab5", "A-5", "Bb5", "B-5",
  "C-6", "C#6", "D-6", "Eb6", "E-6", "F-6", "F#6", "G-6", "Ab6", "A-6", "Bb6", "B-6",
  "C-7", "C#7", "D-7", "Eb7", "E-7", "F-7", "F#7", "G-7", "Ab7", "A-7", "Bb7", "B-7",
  "C-8", "C#8", "D-8", "Eb8", "E-8", "F-8", "F#8", "G-8", "Ab8", "A-8", "Bb8", "B-8",
  "C-9", "C#9", "D-9", "Eb9", "E-9", "F-9", "F#9", "G-9", "Ab9", "A-9", "Bb9", "B-9",
  "C-A", "C#A", "D-A", "EbA", "E-A", "F-A", "F#A", "G-A", "AbA", "A-A", "BbA", "B-A",
  "C-B", "C#B", "D-B", "EbB", "E-B", "F-B", "F#B", "G-B", "AbB", "A-B", "BbB", "B-B",
  "C-C", "C#C", "D-C", "EbC", "E-C", "F-C", "F#C", "G-C", "AbC", "A-C", "BbC", "B-C",
  "C-D", "C#D", "D-D", "EbD", "E-D", "F-D", "F#D", "G-D", "AbD", "A-D", "BbD", "B-D",
  "C-E", "C#E", "D-E", "EbE", "E-E", "F-E", "F#E", "G-E", "AbE", "A-E", "BbE", "B-E",
  "C-F", "C#F", "D-F", "EbF", "E-F", "F-F", "F#F", "G-F", "AbF", "A-F", "BbF", "B-F",
  "???", "???", "???", "???", "???", "???", "???", "???", "???", "???", "???", "???",
  "???", "???", "???", "???", "???", "???", "???", "???", "???", "???", "???", "???",
  "???", "???", "???", "???", "???", "???", "???", "???", "???", "???", "???", "???",
  "???", "???", "???", "???", "???", "???", "???", "???", "???", "???", "???", "???",
  "???", "???", "???", "???", "???", "???", "???", "???", "???", "???", "???", "???",
  "???", "???", "???", "???"
};

char fx_strings[256][3] = {
  "--", "01", "02", "03", "04", "05", "06", "07",
  "08", "09", "0A", "0B", "0C", "0D", "0E", "0F",
  "10", "11", "12", "13", "14", "15", "16", "17",
  "18", "19", "1A", "1B", "1C", "1D", "1E", "1F",
  "20", "21", "22", "23", "24", "25", "26", "27",
  "28", "29", "2A", "2B", "2C", "2D", "2E", "2F",
  "30", "31", "32", "33", "34", "35", "36", "37",
  "38", "39", "3A", "3B", "3C", "3D", "3E", "3F",
  "40", "41", "42", "43", "44", "45", "46", "47",
  "48", "49", "4A", "4B", "4C", "4D", "4E", "4F",
  "50", "51", "52", "53", "54", "55", "56", "57",
  "58", "59", "5A", "5B", "5C", "5D", "5E", "5F",
  "60", "61", "62", "63", "64", "65", "66", "67",
  "68", "69", "6A", "6B", "6C", "6D", "6E", "6F",
  "70", "71", "72", "73", "74", "75", "76", "77",
  "78", "79", "7A", "7B", "7C", "7D", "7E", "7F",
  "80", "81", "82", "83", "84", "85", "86", "87",
  "88", "89", "8A", "8B", "8C", "8D", "8E", "8F",
  "90", "91", "92", "93", "94", "95", "96", "97",
  "98", "99", "9A", "9B", "9C", "9D", "9E", "9F",
  "A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7",
  "A8", "A9", "AA", "AB", "AC", "AD", "AE", "AF",
  "B0", "B1", "B2", "B3", "B4", "B5", "B6", "B7",
  "B8", "B9", "BA", "BB", "BC", "BD", "BE", "BF",
  "C0", "C1", "C2", "C3", "C4", "C5", "C6", "C7",
  "C8", "C9", "CA", "CB", "CC", "CD", "CE", "CF",
  "D0", "D1", "D2", "D3", "D4", "D5", "D6", "D7",
  "D8", "D9", "DA", "DB", "DC", "DD", "DE", "DF",
  "E0", "E1", "E2", "E3", "E4", "E5", "E6", "E7",
  "E8", "E9", "EA", "EB", "EC", "ED", "EE", "EF",
  "F0", "F1", "F2", "F3", "F4", "F5", "F6", "F7",
  "F8", "F9", "FA", "FB", "FC", "FD", "FE", "FF"
};
