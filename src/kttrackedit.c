/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

static char *_id = "$Id$";

#include <ctype.h>
#include <string.h>
#include <gtk/gtk.h>
#include "gdk/gdkkeysyms.h"
/*
#include "gtkmain.h"
#include "gtksignal.h"
*/
#include <xmp-kt/xmp.h>
#include <xmp-kt/xmpi.h>
#include <xmp-kt/xxm.h>
#include "rowstrings.h"
#include "kttrackedit.h"


#define INITIAL_BUFFER_SIZE      1024
#define INITIAL_LINE_CACHE_SIZE  256
#define MIN_GAP_SIZE             256
#define LINE_DELIM               '\n'
#define MIN_TEXT_WIDTH_LINES     20
#define MIN_TEXT_HEIGHT_LINES    10
#define TEXT_BORDER_ROOM         3
#define LINE_WRAP_ROOM           8           /* The bitmaps are 6 wide. */
#define DEFAULT_TAB_STOP_WIDTH   4
#define SCROLL_PIXELS            5
#define KEY_SCROLL_PIXELS        10

#define TEXT_INDEX(t, index)        ((index) < (t)->gap_position ? (t)->text[index] : \
				     (t)->text[(index) + (t)->gap_size])
#define TEXT_LENGTH(t)              ((t)->text_end - (t)->gap_size)
#define FONT_HEIGHT(f)              ((f)->ascent + (f)->descent)
#define LINE_HEIGHT(l)              ((l).font_ascent + (l).font_descent)
#define LINE_CONTAINS(l, i)         ((l).start.index <= (i) && (l).end.index >= (i))
#define LINE_STARTS_AT(l, i)        ((l).start.index == (i))
#define LINE_START_PIXEL(l)         ((l).tab_cont.pixel_offset)
#define LAST_INDEX(t, m)            ((m).index == TEXT_LENGTH(t))
#define CACHE_DATA(c)               (*(LineParams*)(c)->data)


typedef struct _TextFont              TextFont;
typedef struct _TextProperty          TextProperty;
typedef struct _TabStopMark           TabStopMark;
typedef struct _PrevTabCont           PrevTabCont;
typedef struct _FetchLinesData        FetchLinesData;
typedef struct _LineParams            LineParams;
typedef struct _SetVerticalScrollData SetVerticalScrollData;

typedef gint (*LineIteratorFunction) (KTTrackEdit* text, LineParams* lp, void* data);

typedef enum
{
  FetchLinesPixels,
  FetchLinesCount
} FLType;

struct _SetVerticalScrollData {
  gint pixel_height;
  gint last_didnt_wrap;
  gint last_line_start;
  GtkPropertyMark mark;
};

struct _TextFont
{
  /* The actual font. */
  GdkFont *gdk_font;

  gint16 char_widths[256];
};

struct _TextProperty
{
  /* Font. */
  TextFont* font;

  /* Background Color. */
  GdkColor* back_color;

  /* Foreground Color. */
  GdkColor* fore_color;

  /* Length of this property. */
  guint length;
};

struct _TabStopMark
{
  GList* tab_stops; /* Index into list containing the next tab position.  If
		     * NULL, using default widths. */
  gint to_next_tab;
};

struct _PrevTabCont
{
  guint pixel_offset;
  TabStopMark tab_start;
};

struct _FetchLinesData
{
  GList* new_lines;
  FLType fl_type;
  gint data;
  gint data_max;
};

struct _LineParams
{
  guint font_ascent;
  guint font_descent;
  guint pixel_width;
  guint displayable_chars;
  guint wraps : 1;

  PrevTabCont tab_cont;
  PrevTabCont tab_cont_next;

  GtkPropertyMark start;
  GtkPropertyMark end;
};


static void  kt_trackedit_class_init     (KTTrackEditClass   *klass);
static void  kt_trackedit_init           (KTTrackEdit        *text);
static void  kt_trackedit_destroy        (GtkObject      *object);
static void  kt_trackedit_realize        (GtkWidget      *widget);
static void  kt_trackedit_unrealize      (GtkWidget      *widget);
static void  kt_trackedit_draw_focus     (GtkWidget      *widget);
static void  kt_trackedit_size_request   (GtkWidget      *widget,
				      GtkRequisition *requisition);
static void  kt_trackedit_size_allocate  (GtkWidget      *widget,
				      GtkAllocation  *allocation);
static void  kt_trackedit_adjustment     (GtkAdjustment  *adjustment,
				      KTTrackEdit        *text);
static void  kt_trackedit_disconnect     (GtkAdjustment  *adjustment,
				      KTTrackEdit        *text);

/* Event handlers */
static void  kt_trackedit_draw              (GtkWidget         *widget,
					 GdkRectangle      *area);
static gint  kt_trackedit_expose            (GtkWidget         *widget,
					 GdkEventExpose    *event);
static gint  kt_trackedit_button_press      (GtkWidget         *widget,
					 GdkEventButton    *event);
static gint  kt_trackedit_button_release    (GtkWidget         *widget,
					 GdkEventButton    *event);
static gint  kt_trackedit_motion_notify     (GtkWidget         *widget,
					 GdkEventMotion    *event);
static gint  kt_trackedit_key_press         (GtkWidget         *widget,
					 GdkEventKey       *event);
static gint  kt_trackedit_focus_in          (GtkWidget         *widget,
					 GdkEventFocus     *event);
static gint  kt_trackedit_focus_out         (GtkWidget         *widget,
				         GdkEventFocus     *event);
static gint  kt_trackedit_selection_clear   (GtkWidget         *widget,
					 GdkEventSelection *event);
static gint  kt_trackedit_selection_request (GtkWidget         *widget,
					 GdkEventSelection *event);
static gint  kt_trackedit_selection_notify  (GtkWidget         *widget,
					 GdkEventSelection *event);

/* Display */
void update_pattern (KTTrackEdit *ktt, int pattern);
#if 0
static LineParams find_line_params (KTTrackEdit* text,
				    const GtkPropertyMark *mark,
				    const PrevTabCont *tab_cont,
				    PrevTabCont *next_cont);
#endif
static void recompute_geometry (KTTrackEdit* text);
static void clear_area (KTTrackEdit *text, GdkRectangle *area);
static void draw_line (KTTrackEdit* text,
		       gint pixel_height,
                       int pat,
		       int lineno);
static void draw_line_wrap (KTTrackEdit* text,
			    guint height);
static void draw_cursor (KTTrackEdit* text, gint absolute);
static void undraw_cursor (KTTrackEdit* text, gint absolute);
static gint drawn_cursor_min (KTTrackEdit* text);
static gint drawn_cursor_max (KTTrackEdit* text);
static void expose_text (KTTrackEdit* text, GdkRectangle *area, gboolean cursor);

/* Scrolling. */
static void adjust_adj  (KTTrackEdit* text, GtkAdjustment* adj);
static void scroll_up   (KTTrackEdit* text, gint diff);
static void scroll_down (KTTrackEdit* text, gint diff);
static void scroll_int  (KTTrackEdit* text, gint diff);

static void process_exposes (KTTrackEdit *text);

/* Cache Management. */
static GList* remove_cache_line (KTTrackEdit* text, GList* list);

/* Key Motion. */
static void move_cursor_buffer_ver (KTTrackEdit *text, int dir);
static void move_cursor_page_ver (KTTrackEdit *text, int dir);
static void move_cursor_ver (KTTrackEdit *text, int count);
static void move_cursor_hor (KTTrackEdit *text, int count);

/* #define DEBUG_KTTRACKEDIT */

#if defined(DEBUG_KTTRACKEDIT) && defined(__GNUC__)
/* Debugging utilities. */
static void kt_trackedit_assert_mark (KTTrackEdit         *text,
				  GtkPropertyMark *mark,
				  GtkPropertyMark *before,
				  GtkPropertyMark *after,
				  const gchar     *msg,
				  const gchar     *where,
				  gint             line);

static void kt_trackedit_assert (KTTrackEdit         *text,
			     const gchar     *msg,
			     gint             line);
static void kt_trackedit_show_cache_line (KTTrackEdit *text, GList *cache,
				      const char* what, const char* func, gint line);
static void kt_trackedit_show_cache (KTTrackEdit *text, const char* func, gint line);
static void kt_trackedit_show_adj (KTTrackEdit *text,
			       GtkAdjustment *adj,
			       const char* what,
			       const char* func,
			       gint line);
static void kt_trackedit_show_props (KTTrackEdit* test,
				 const char* func,
				 int line);

#define TDEBUG(args) g_print args
#define TEXT_ASSERT(text) kt_trackedit_assert (text,__PRETTY_FUNCTION__,__LINE__)
#define TEXT_ASSERT_MARK(text,mark,msg) kt_trackedit_assert_mark (text,mark, \
					   __PRETTY_FUNCTION__,msg,__LINE__)
#define TEXT_SHOW(text) kt_trackedit_show_cache (text, __PRETTY_FUNCTION__,__LINE__)
#define TEXT_SHOW_LINE(text,line,msg) kt_trackedit_show_cache_line (text,line,msg,\
					   __PRETTY_FUNCTION__,__LINE__)
#define TEXT_SHOW_ADJ(text,adj,msg) kt_trackedit_show_adj (text,adj,msg, \
					  __PRETTY_FUNCTION__,__LINE__)
#else
#define TDEBUG(args)
#define TEXT_ASSERT(text)
#define TEXT_ASSERT_MARK(text,mark,msg)
#define TEXT_SHOW(text)
#define TEXT_SHOW_LINE(text,line,msg)
#define TEXT_SHOW_ADJ(text,adj,msg)
#endif

/* Memory Management. */
static GMemChunk  *params_mem_chunk    = NULL;
static GMemChunk  *text_property_chunk = NULL;

static GtkWidgetClass *parent_class = NULL;


/**********************************************************************/
/*			        Widget Crap                           */
/**********************************************************************/

guint
kt_trackedit_get_type ()
{
  static guint text_type = 0;

  if (!text_type)
    {
      GtkTypeInfo text_info =
      {
	"KTTrackEdit",
	sizeof (KTTrackEdit),
	sizeof (KTTrackEditClass),
	(GtkClassInitFunc) kt_trackedit_class_init,
	(GtkObjectInitFunc) kt_trackedit_init,
	(GtkArgSetFunc) NULL,
        (GtkArgGetFunc) NULL,
      };

      text_type = gtk_type_unique (gtk_widget_get_type (), &text_info);
    }

  return text_type;
}

static void
kt_trackedit_class_init (KTTrackEditClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;

  parent_class = gtk_type_class (gtk_widget_get_type ());

  object_class->destroy = kt_trackedit_destroy;

  widget_class->realize = kt_trackedit_realize;
  widget_class->unrealize = kt_trackedit_unrealize;
  widget_class->draw_focus = kt_trackedit_draw_focus;
  widget_class->size_request = kt_trackedit_size_request;
  widget_class->size_allocate = kt_trackedit_size_allocate;
  widget_class->draw = kt_trackedit_draw;
  widget_class->expose_event = kt_trackedit_expose;
  widget_class->button_press_event = kt_trackedit_button_press;
  widget_class->button_release_event = kt_trackedit_button_release;
  widget_class->motion_notify_event = kt_trackedit_motion_notify;
  widget_class->key_press_event = kt_trackedit_key_press;
  widget_class->focus_in_event = kt_trackedit_focus_in;
  widget_class->focus_out_event = kt_trackedit_focus_out;
  widget_class->selection_clear_event = kt_trackedit_selection_clear;
  widget_class->selection_request_event = kt_trackedit_selection_request;
  widget_class->selection_notify_event = kt_trackedit_selection_notify;
}

static void
kt_trackedit_init (KTTrackEdit *text)
{
  GTK_WIDGET_SET_FLAGS (text, GTK_CAN_FOCUS);

  text->text = g_new (guchar, INITIAL_BUFFER_SIZE);
  text->text_len = INITIAL_BUFFER_SIZE;

  if (!params_mem_chunk)
    params_mem_chunk = g_mem_chunk_new ("LineParams",
					sizeof (LineParams),
					256 * sizeof (LineParams),
					G_ALLOC_AND_FREE);

  text->default_tab_width = 4;
  text->tab_stops = NULL;

  text->tab_stops = g_list_prepend (text->tab_stops, (void*)8);
  text->tab_stops = g_list_prepend (text->tab_stops, (void*)8);

  text->line_wrap = TRUE;
  text->is_editable = FALSE;
}

GtkWidget*
kt_trackedit_new (int channel, GtkAdjustment *hadj,
	      GtkAdjustment *vadj)
{
  KTTrackEdit *text;

  text = gtk_type_new (kt_trackedit_get_type ());

  text->channel = channel;
  text->current_pattern = xxo[0];

  kt_trackedit_set_adjustments (text, hadj, vadj);

  return GTK_WIDGET (text);
}

void
kt_trackedit_set_editable (KTTrackEdit *text,
		       gint     editable)
{
  g_return_if_fail (text != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (text));

  text->is_editable = (editable != FALSE);
}

void
kt_trackedit_set_adjustments (KTTrackEdit       *text,
			  GtkAdjustment *hadj,
			  GtkAdjustment *vadj)
{
  g_return_if_fail (text != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (text));

  if (text->hadj && (text->hadj != hadj))
    {
      gtk_signal_disconnect_by_data (GTK_OBJECT (text->hadj), text);
      gtk_object_unref (GTK_OBJECT (text->hadj));
    }

  if (text->vadj && (text->vadj != vadj))
    {
      gtk_signal_disconnect_by_data (GTK_OBJECT (text->vadj), text);
      gtk_object_unref (GTK_OBJECT (text->vadj));
    }

  if (!hadj)
    hadj = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0));

  if (!vadj)
    vadj = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0));

  if (text->hadj != hadj)
    {
      text->hadj = hadj;
      gtk_object_ref (GTK_OBJECT (text->hadj));

      gtk_signal_connect (GTK_OBJECT (text->hadj), "changed",
			  (GtkSignalFunc) kt_trackedit_adjustment,
			  text);
      gtk_signal_connect (GTK_OBJECT (text->hadj), "value_changed",
			  (GtkSignalFunc) kt_trackedit_adjustment,
			  text);
      gtk_signal_connect (GTK_OBJECT (text->hadj), "disconnect",
			  (GtkSignalFunc) kt_trackedit_disconnect,
			  text);
    }

  if (text->vadj != vadj)
    {
      text->vadj = vadj;
      gtk_object_ref (GTK_OBJECT (text->vadj));

      gtk_signal_connect (GTK_OBJECT (text->vadj), "changed",
			  (GtkSignalFunc) kt_trackedit_adjustment,
			  text);
      gtk_signal_connect (GTK_OBJECT (text->vadj), "value_changed",
			  (GtkSignalFunc) kt_trackedit_adjustment,
			  text);
      gtk_signal_connect (GTK_OBJECT (text->vadj), "disconnect",
			  (GtkSignalFunc) kt_trackedit_disconnect,
			  text);
    }
}

void
kt_trackedit_set_point (KTTrackEdit *text,
		    guint    index)
{
  g_return_if_fail (text != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (text));
  g_return_if_fail (index >= 0 && index <= TEXT_LENGTH (text));

  /*text->point = find_mark (text, index);*/
}

guint
kt_trackedit_get_point (KTTrackEdit *text)
{
  g_return_val_if_fail (text != NULL, 0);
  g_return_val_if_fail (IS_KTTRACKEDIT (text), 0);

  return text->point.index;
}

guint
kt_trackedit_get_length (KTTrackEdit *text)
{
  g_return_val_if_fail (text != NULL, 0);
  g_return_val_if_fail (IS_KTTRACKEDIT (text), 0);

  return TEXT_LENGTH (text);
}

void
kt_trackedit_freeze (KTTrackEdit *text)
{
  g_return_if_fail (text != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (text));

  text->freeze = TRUE;
}

void
kt_trackedit_thaw (KTTrackEdit *text)
{
  g_return_if_fail (text != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (text));

  text->freeze = FALSE;

  if (GTK_WIDGET_DRAWABLE (text))
    {
      recompute_geometry (text);
      gtk_widget_queue_draw (GTK_WIDGET (text));
    }
}

void
kt_trackedit_insert (KTTrackEdit    *text,
		 GdkFont    *font,
		 GdkColor   *fore,
		 GdkColor   *back,
		 const char *chars,
		 gint        length)
{
  g_return_if_fail (text != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (text));

  g_assert (GTK_WIDGET_REALIZED (text));

  /* back may be NULL, fore may not. */
  if (fore == NULL)
    fore = &text->widget.style->fg[GTK_STATE_NORMAL];

  /* This must be because we need to have the style set up. */
  g_assert (GTK_WIDGET_REALIZED(text));

  if (length < 0)
    length = strlen (chars);

  if (length == 0)
    return;

  /*move_gap_to_point (text);*/

  if (font == NULL)
    font = GTK_WIDGET (text)->style->font;

  /*make_forward_space (text, length);*/

  memcpy (text->text + text->gap_position, chars, length);

  /*insert_text_property (text, font, fore, back, length);*/

  text->gap_size -= length;
  text->gap_position += length;

  /*advance_mark_n (&text->point, length);*/
}

gint
kt_trackedit_backward_delete (KTTrackEdit *text,
			  guint    nchars)
{
  g_return_val_if_fail (text != NULL, 0);
  g_return_val_if_fail (IS_KTTRACKEDIT (text), 0);

  if (nchars > text->point.index || nchars <= 0)
    return FALSE;

  kt_trackedit_set_point (text, text->point.index - nchars);

  return kt_trackedit_forward_delete (text, nchars);
}

gint
kt_trackedit_forward_delete (KTTrackEdit *text,
			  guint    nchars)
{
  g_return_val_if_fail (text != NULL, 0);
  g_return_val_if_fail (IS_KTTRACKEDIT (text), 0);

  if (text->point.index + nchars > TEXT_LENGTH (text) || nchars <= 0)
    return FALSE;

  /*move_gap_to_point (text);*/

  text->gap_size += nchars;

  /*delete_text_property (text, nchars);*/

  return TRUE;
}

static void
kt_trackedit_destroy (GtkObject *object)
{
  g_return_if_fail (object != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (object));

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
kt_trackedit_realize (GtkWidget *widget)
{
  KTTrackEdit *text;
  GdkWindowAttr attributes;
  gint attributes_mask;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (widget));

  text = (KTTrackEdit*) widget;
  GTK_WIDGET_SET_FLAGS (text, GTK_REALIZED);

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget);
  attributes.event_mask |= (GDK_EXPOSURE_MASK |
			    GDK_BUTTON_PRESS_MASK |
			    GDK_BUTTON_RELEASE_MASK |
			    GDK_BUTTON_MOTION_MASK |
			    GDK_ENTER_NOTIFY_MASK |
			    GDK_LEAVE_NOTIFY_MASK |
			    GDK_KEY_PRESS_MASK);
  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
  gdk_window_set_user_data (widget->window, text);

  attributes.x = (widget->style->klass->xthickness + TEXT_BORDER_ROOM);
  attributes.y = (widget->style->klass->ythickness + TEXT_BORDER_ROOM);
  attributes.width = widget->allocation.width - attributes.x * 2;
  attributes.height = widget->allocation.height - attributes.y * 2;

  text->text_area = gdk_window_new (widget->window, &attributes, attributes_mask);
  gdk_window_set_user_data (text->text_area, text);

  widget->style = gtk_style_attach (widget->style, widget->window);

  /* Can't call gtk_style_set_background here because its handled specially */
  if (!text->widget.style->bg_pixmap[GTK_STATE_NORMAL])
    gdk_window_set_background(text->widget.window, &text->widget.style->white);
    /*gdk_window_set_background (text->widget.window, &text->widget.style->bg[GTK_STATE_NORMAL]);*/

  if (!text->widget.style->bg_pixmap[GTK_STATE_NORMAL])
    gdk_window_set_background(text->text_area, &text->widget.style->white);
    /*gdk_window_set_background (text->text_area, &text->widget.style->bg[GTK_STATE_NORMAL]);*/

  text->gc = gdk_gc_new (text->text_area);
  gdk_gc_set_exposures (text->gc, TRUE);
  gdk_gc_set_foreground (text->gc, &widget->style->fg[GTK_STATE_NORMAL]);

  /*init_properties (text);*/

  gdk_window_show (text->text_area);
}

static void
kt_trackedit_unrealize (GtkWidget *widget)
{
  KTTrackEdit *text;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (widget));

  text = KTTRACKEDIT (widget);
  GTK_WIDGET_UNSET_FLAGS (widget, GTK_REALIZED | GTK_MAPPED);

  gtk_style_detach (widget->style);
  gdk_window_destroy (widget->window);
  gdk_window_destroy (text->text_area);
  gdk_gc_destroy (text->gc);

  widget->window = NULL;
  text->text_area = NULL;
  text->gc = NULL;
}

static void
clear_focus_area (KTTrackEdit *text, gint area_x, gint area_y, gint area_width, gint area_height)
{
  gint ythick = TEXT_BORDER_ROOM + text->widget.style->klass->ythickness;
  gint xthick = TEXT_BORDER_ROOM + text->widget.style->klass->xthickness;

  gint width, height;
  gint xorig, yorig;
  gint x, y;

  gdk_window_get_size (text->widget.style->bg_pixmap[GTK_STATE_NORMAL], &width, &height);

  yorig = - text->first_onscreen_ver_pixel + ythick;
  xorig = - text->first_onscreen_hor_pixel + xthick;

  while (yorig > 0)
    yorig -= height;

  while (xorig > 0)
    xorig -= width;

  for (y = area_y; y < area_y + area_height; )
    {
      gint yoff = (y - yorig) % height;
      gint yw = MIN(height - yoff, (area_y + area_height) - y);

      for (x = area_x; x < area_x + area_width; )
	{
	  gint xoff = (x - xorig) % width;
	  gint xw = MIN(width - xoff, (area_x + area_width) - x);

	  gdk_draw_pixmap (text->widget.window,
			   text->gc,
			   text->widget.style->bg_pixmap[GTK_STATE_NORMAL],
			   xoff,
			   yoff,
			   x,
			   y,
			   xw,
			   yw);

	  x += width - xoff;
	}
      y += height - yoff;
    }
}

static void
kt_trackedit_draw_focus (GtkWidget *widget)
{
  KTTrackEdit *text;
  gint width, height;
  gint x, y;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (widget));

  text = KTTRACKEDIT (widget);

  if (GTK_WIDGET_DRAWABLE (widget))
    {
      TDEBUG (("in kt_trackedit_draw_focus\n"));

      x = 0;
      y = 0;
      width = widget->allocation.width;
      height = widget->allocation.height;

      if (widget->style->bg_pixmap[GTK_STATE_NORMAL])
	{
	  gint ythick = TEXT_BORDER_ROOM + widget->style->klass->ythickness;
	  gint xthick = TEXT_BORDER_ROOM + widget->style->klass->xthickness;

	  /* top rect */
	  clear_focus_area (text, 0, 0, width, ythick);
	  /* right rect */
	  clear_focus_area (text, 0, ythick, xthick, height - 2 * ythick);
	  /* left rect */
	  clear_focus_area (text, width - xthick, ythick, xthick, height - 2 * ythick);
	  /* bottom rect */
	  clear_focus_area (text, 0, height - ythick, width, ythick);
	}

      if (GTK_WIDGET_HAS_FOCUS (widget))
	{
	  x += 1;
	  y += 1;
	  width -=  2;
	  height -= 2;

	  gdk_draw_rectangle (widget->window,
			      widget->style->fg_gc[GTK_STATE_NORMAL],
			      FALSE, 0, 0,
			      widget->allocation.width - 1,
			      widget->allocation.height - 1);
	}
      else
	{
	  gdk_draw_rectangle (widget->window,
			      widget->style->white_gc, FALSE,
			      x + 2,
			      y + 2,
			      width - 1 - 2,
			      height - 1 - 2);
	}

      gtk_draw_shadow (widget->style, widget->window,
		       GTK_STATE_NORMAL, GTK_SHADOW_IN,
		       x, y, width, height);
    }
  else
    {
      TDEBUG (("in kt_trackedit_draw_focus (undrawable !!!)\n"));
    }
}

static void
kt_trackedit_size_request (GtkWidget      *widget,
		       GtkRequisition *requisition)
{
  gint xthickness;
  gint ythickness;
  gint char_height;
  gint char_width;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (widget));
  g_return_if_fail (requisition != NULL);

  xthickness = widget->style->klass->xthickness + TEXT_BORDER_ROOM;
  ythickness = widget->style->klass->ythickness + TEXT_BORDER_ROOM;

  char_height = MIN_TEXT_HEIGHT_LINES * (widget->style->font->ascent +
					 widget->style->font->descent);

  char_width = MIN_TEXT_WIDTH_LINES * (gdk_text_width (widget->style->font,
						       "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
						       26)
				       / 26);

  requisition->width  = char_width  + xthickness * 2;
  requisition->height = char_height + ythickness * 2;
}

static void
kt_trackedit_size_allocate (GtkWidget     *widget,
			GtkAllocation *allocation)
{
  KTTrackEdit *text;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (widget));
  g_return_if_fail (allocation != NULL);

  text = KTTRACKEDIT (widget);

  widget->allocation = *allocation;
  if (GTK_WIDGET_REALIZED (widget))
    {
      gdk_window_move_resize (widget->window,
			      allocation->x, allocation->y,
			      allocation->width, allocation->height);

      gdk_window_move_resize (text->text_area,
			      widget->style->klass->xthickness + TEXT_BORDER_ROOM,
			      widget->style->klass->ythickness + TEXT_BORDER_ROOM,
			      widget->allocation.width - (widget->style->klass->xthickness +
							  TEXT_BORDER_ROOM) * 2,
			      widget->allocation.height - (widget->style->klass->ythickness +
							   TEXT_BORDER_ROOM) * 2);

      recompute_geometry (text);
    }
}

static void
kt_trackedit_draw (GtkWidget    *widget,
	       GdkRectangle *area)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (widget));
  g_return_if_fail (area != NULL);

  if (GTK_WIDGET_DRAWABLE (widget))
    {
      expose_text (KTTRACKEDIT (widget), area, TRUE);
      gtk_widget_draw_focus (widget);
    }
}

static gint
kt_trackedit_expose (GtkWidget      *widget,
		 GdkEventExpose *event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_KTTRACKEDIT (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  if (event->window == KTTRACKEDIT (widget)->text_area)
    {
      TDEBUG (("in kt_trackedit_expose (expose)\n"));
      expose_text (KTTRACKEDIT (widget), &event->area, TRUE);
    }
  else if (event->count == 0)
    {
      TDEBUG (("in kt_trackedit_expose (focus)\n"));
      gtk_widget_draw_focus (widget);
    }

  return FALSE;
}

static gint
kt_trackedit_button_press (GtkWidget      *widget,
		       GdkEventButton *event)
{
  KTTrackEdit *text;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_KTTRACKEDIT (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  text = KTTRACKEDIT(widget);
  if (!GTK_WIDGET_HAS_FOCUS (widget))
    gtk_widget_grab_focus (widget);

  if (event->type == GDK_BUTTON_PRESS && event->button != 2)
    gtk_grab_add (widget);

  if (event->button == 1)
    {
      switch (event->type)
	{
	case GDK_BUTTON_PRESS:
	  undraw_cursor (KTTRACKEDIT (widget), FALSE);
	  /*mouse_click_1 (KTTRACKEDIT (widget), event);*/
	  draw_cursor (KTTRACKEDIT (widget), FALSE);
	  /* start selection */
	  break;

	case GDK_2BUTTON_PRESS:
	  /* select word */
	  break;

	case GDK_3BUTTON_PRESS:
	  /* select line */
	  break;

	default:
	  break;
	}
    }
  else if (event->type == GDK_BUTTON_PRESS)
    {
      if (event->button == 2)
	{
	  /* insert selection. */
	}
      else
	{
	  /* start selection */
	}
    }

  return FALSE;
}

static gint
kt_trackedit_button_release (GtkWidget      *widget,
			 GdkEventButton *event)
{
  KTTrackEdit *text;
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_KTTRACKEDIT (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  if (event->button != 2)
    {
      gtk_grab_remove (widget);

      text = KTTRACKEDIT (widget);

      /* stop selecting. */
    }

  return FALSE;
}

static gint
kt_trackedit_motion_notify (GtkWidget      *widget,
			 GdkEventMotion *event)
{
  KTTrackEdit *text;
  gint x;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_KTTRACKEDIT (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  text = KTTRACKEDIT (widget);

  x = event->x;
  if (event->is_hint || (text->text_area != event->window))
    gdk_window_get_pointer (text->text_area, &x, NULL, NULL);

  /* update selection */

  return FALSE;
}

static void
kt_trackedit_insert_1_at_point (KTTrackEdit* text, char key)
{
#if 0
  kt_trackedit_insert (text,
		   MARK_CURRENT_FONT (&text->point),
		   MARK_CURRENT_FORE (&text->point),
		   MARK_CURRENT_BACK (&text->point),
		   &key, 1);

  insert_char_line_expose (text, key, 
			   total_line_height (text, text->current_line, 1));
#endif

}

static void
kt_trackedit_backward_delete_1_at_point (KTTrackEdit* text, char key)
{
#if 0
  kt_trackedit_backward_delete (text, 1);

  delete_char_line_expose (text, key, 
			   total_line_height (text, text->current_line, 1));
#endif
}

static void
kt_trackedit_forward_delete_1_at_point (KTTrackEdit* text, char key)
{
#if 0
  kt_trackedit_forward_delete (text, 1);

  delete_char_line_expose (text, key, 
			   total_line_height (text, text->current_line, 1));
#endif
}

static gint
kt_trackedit_key_press (GtkWidget   *widget,
		    GdkEventKey *event)
{
  KTTrackEdit *text;
  gchar key;
  gint return_val;
  struct xxm_event *xe;
  struct xxm_trackinfo *tinfo;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_KTTRACKEDIT (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  if(!xxp) return;
  tinfo = xxp[text->current_pattern]->info;

  return_val = TRUE;

  text = KTTRACKEDIT (widget);

  xe=&((xxt[tinfo[text->channel].index]->event)[text->current_row]);
  xe->note = 24;

  if (!return_val)
    {
      key = event->keyval;
      return_val = TRUE;

      if (text->is_editable == FALSE)
	{
	  switch (event->keyval)
	    {
	    case GDK_Home:      scroll_int (text, -text->vadj->value); break;
	    case GDK_End:       scroll_int (text, +text->vadj->upper); break;
	    case GDK_Page_Up:   scroll_int (text, -text->vadj->page_increment); break;
	    case GDK_Page_Down: scroll_int (text, +text->vadj->page_increment); break;
	    case GDK_Up:        scroll_int (text, -KEY_SCROLL_PIXELS); break;
	    case GDK_Down:      scroll_int (text, +KEY_SCROLL_PIXELS); break;
	    default: break;
	    }
	}
      else
	{
	  /*text->point = find_mark (text, text->cursor_mark.index);*/

	  switch (event->keyval)
	    {
	    case GDK_Home:      move_cursor_buffer_ver (text, -1); break;
	    case GDK_End:       move_cursor_buffer_ver (text, +1); break;
	    case GDK_Page_Up:   move_cursor_page_ver (text, -1); break;
	    case GDK_Page_Down: move_cursor_page_ver (text, +1); break;
	    case GDK_Up:        move_cursor_ver (text, -1); break;
	    case GDK_Down:      move_cursor_ver (text, +1); break;
	    case GDK_Left:      move_cursor_hor (text, -1); break;
	    case GDK_Right:     move_cursor_hor (text, +1); break;

	    case GDK_BackSpace:
	      if (!text->has_cursor || text->cursor_mark.index == 0)
		break;

	      kt_trackedit_backward_delete_1_at_point (text, key);
	      break;
	    case GDK_Delete:
	      if (!text->has_cursor || LAST_INDEX (text, text->cursor_mark))
		break;

	      kt_trackedit_forward_delete_1_at_point (text, key);
	      break;
	    case GDK_Tab:
	      if (!text->has_cursor)
		break;

	      kt_trackedit_insert_1_at_point (text, '\t');
	      break;
	    case GDK_Return:
	      if (!text->has_cursor)
		break;

	      kt_trackedit_insert_1_at_point (text, '\n');
	      break;
	    default:
	      if (!text->has_cursor)
		break;

	      if ((event->keyval >= 0x20) && (event->keyval <= 0x7e))
		{
		  return_val = TRUE;

		  if (event->state & GDK_CONTROL_MASK)
		    {
		      if ((key >= 'A') && (key <= 'Z'))
			key -= 'A' - 'a';

		      if ((key >= 'a') && (key <= 'z') && text->control_keys[(int) (key - 'a')])
			(* text->control_keys[(int) (key - 'a')]) (text);
		    }
		  else if (event->state & GDK_MOD1_MASK)
		    {
		      g_message ("alt key");

		      if ((key >= 'A') && (key <= 'Z'))
			key -= 'A' - 'a';

		      if ((key >= 'a') && (key <= 'z') && text->alt_keys[(int) (key - 'a')])
			(* text->alt_keys[(int) (key - 'a')]) (text);
		    }
		  else
		    {
		      kt_trackedit_insert_1_at_point (text, key);
		    }
		}
	      else
		{
		  return_val = FALSE;
		}
	      break;
	    }
	}
    }

  return return_val;
}

static gint
kt_trackedit_focus_in (GtkWidget     *widget,
		   GdkEventFocus *event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_KTTRACKEDIT (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  TDEBUG (("in kt_trackedit_focus_in\n"));

  GTK_WIDGET_SET_FLAGS (widget, GTK_HAS_FOCUS);
  gtk_widget_draw_focus (widget);

  draw_cursor (KTTRACKEDIT(widget), TRUE);

  return FALSE;
}

static gint
kt_trackedit_focus_out (GtkWidget     *widget,
		    GdkEventFocus *event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_KTTRACKEDIT (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  TDEBUG (("in kt_trackedit_focus_out\n"));

  GTK_WIDGET_UNSET_FLAGS (widget, GTK_HAS_FOCUS);
  gtk_widget_draw_focus (widget);

  undraw_cursor (KTTRACKEDIT(widget), TRUE);

  return FALSE;
}

static void
kt_trackedit_adjustment (GtkAdjustment *adjustment,
		     KTTrackEdit       *text)
{
  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (GTK_IS_ADJUSTMENT (adjustment));
  g_return_if_fail (text != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (text));

  if (adjustment == text->hadj)
    {
      g_warning ("horizontal scrolling not implemented");
    }
  else
    {
      gint diff = ((gint)adjustment->value) - text->last_ver_value;

      if (diff != 0)
	{
	  undraw_cursor (text, FALSE);

	  if (diff > 0)
	    scroll_down (text, diff);
	  else /* if (diff < 0) */
	    scroll_up (text, diff);

	  draw_cursor (text, FALSE);

	  text->last_ver_value = adjustment->value;
	}
    }
}

static void
kt_trackedit_disconnect (GtkAdjustment *adjustment,
		     KTTrackEdit       *text)
{
  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (GTK_IS_ADJUSTMENT (adjustment));
  g_return_if_fail (text != NULL);
  g_return_if_fail (IS_KTTRACKEDIT (text));

  if (adjustment == text->hadj)
    text->hadj = NULL;
  if (adjustment == text->vadj)
    text->vadj = NULL;
}


static GtkPropertyMark
find_this_line_start_mark (KTTrackEdit* text, guint point_position, const GtkPropertyMark* near)
{
  GtkPropertyMark mark;

  /*mark = find_mark_near (text, point_position, near);*/

  while (mark.index > 0 &&
	 TEXT_INDEX (text, mark.index - 1) != LINE_DELIM)
    /*decrement_mark (&mark);*/

  return mark;
}

static void
init_tab_cont (KTTrackEdit* text, PrevTabCont* tab_cont)
{
  tab_cont->pixel_offset          = 0;
  tab_cont->tab_start.tab_stops   = text->tab_stops;
  tab_cont->tab_start.to_next_tab = (gulong) text->tab_stops->data;

  if (!tab_cont->tab_start.to_next_tab)
    tab_cont->tab_start.to_next_tab = text->default_tab_width;
}

static void
line_params_iterate (KTTrackEdit* text,
		     const GtkPropertyMark* mark0,
		     const PrevTabCont* tab_mark0,
		     gint8 alloc,
		     void* data,
		     LineIteratorFunction iter)
  /* mark0 MUST be a real line start.  if ALLOC, allocate line params
   * from a mem chunk.  DATA is passed to ITER_CALL, which is called
   * for each line following MARK, iteration continues unless ITER_CALL
   * returns TRUE. */
{
  GtkPropertyMark mark = *mark0;
  PrevTabCont  tab_conts[2];
  LineParams   *lp, lpbuf;
  gint         tab_cont_index = 0;

  if (tab_mark0)
    tab_conts[0] = *tab_mark0;
  else
    init_tab_cont (text, tab_conts);

  for (;;)
    {
      if (alloc)
	lp = g_chunk_new (LineParams, params_mem_chunk);
      else
	lp = &lpbuf;

	/*
      *lp = find_line_params (text, &mark, tab_conts + tab_cont_index,
			      tab_conts + (tab_cont_index + 1) % 2);
	*/

      if ((*iter) (text, lp, data))
	return;

      if (LAST_INDEX (text, lp->end))
	break;

      mark = lp->end;
      /*advance_mark (&mark);*/
      tab_cont_index = (tab_cont_index + 1) % 2;
    }
}

/**********************************************************************/
/*			     Key Motion                               */
/**********************************************************************/

static void
move_cursor_buffer_ver (KTTrackEdit *text, int dir)
{
  if (dir > 0)
    scroll_int (text, text->vadj->upper);
  else
    scroll_int (text, - text->vadj->value);
}

static void
move_cursor_page_ver (KTTrackEdit *text, int dir)
{
  scroll_int (text, dir * text->vadj->page_increment);
}

static void
move_cursor_ver (KTTrackEdit *text, int count)
{
  gint i;
  GtkPropertyMark mark;
  gint offset;

  if (!text->has_cursor)
    {
      scroll_int (text, count * KEY_SCROLL_PIXELS);
      return;
    }

  mark = find_this_line_start_mark (text, text->cursor_mark.index, &text->cursor_mark);
  offset = text->cursor_mark.index - mark.index;

  if (offset > text->cursor_virtual_x)
    text->cursor_virtual_x = offset;

  if (count < 0)
    {
      if (mark.index == 0)
	return;

      /*decrement_mark (&mark);
      mark = find_this_line_start_mark (text, mark.index, &mark);
	*/
    }
  else
    {
      mark = text->cursor_mark;

      while (!LAST_INDEX(text, mark) && TEXT_INDEX(text, mark.index) != LINE_DELIM)
	/*advance_mark (&mark);*/

      if (LAST_INDEX(text, mark))
	  return;

      /*advance_mark (&mark);*/
    }

  for (i=0; i < text->cursor_virtual_x; i += 1/*, advance_mark(&mark)*/)
    if (LAST_INDEX(text, mark) || TEXT_INDEX(text, mark.index) == LINE_DELIM)
      break;

  undraw_cursor (text, FALSE);

  text->cursor_mark = mark;

  /*find_cursor (text);*/

  draw_cursor (text, FALSE);
}

static void
move_cursor_hor (KTTrackEdit *text, int count)
{
  /* count should be +-1. */
  if (!text->has_cursor)
    return;

  if ( (count > 0 && text->cursor_mark.index + count > TEXT_LENGTH(text)) ||
       (count < 0 && text->cursor_mark.index < (- count)) ||
       (count == 0) )
    return;

  text->cursor_virtual_x = 0;

  undraw_cursor (text, FALSE);

  /*move_mark_n (&text->cursor_mark, count);*/

  /*find_cursor (text);*/

  draw_cursor (text, FALSE);
}

/**********************************************************************/
/*			      Scrolling                               */
/**********************************************************************/

static void
adjust_adj (KTTrackEdit* text, GtkAdjustment* adj)
{
  gint height;

  gdk_window_get_size (text->text_area, NULL, &height);

  adj->step_increment = MIN (adj->upper, (float) SCROLL_PIXELS);
  adj->page_increment = MIN (adj->upper, height - (float) KEY_SCROLL_PIXELS);
  adj->page_size      = MIN (adj->upper, height);
  adj->value          = MIN (adj->value, adj->upper - adj->page_size);
  adj->value          = MAX (adj->value, 0.0);

  gtk_signal_emit_by_name (GTK_OBJECT (adj), "changed");
}

static gint
set_vertical_scroll_iterator (KTTrackEdit* text, LineParams* lp, void* data)
{
  gint *pixel_count = (gint*) data;

  if (text->first_line_start_index == lp->start.index)
    text->vadj->value = (float) *pixel_count;

  *pixel_count += LINE_HEIGHT (*lp);

  return FALSE;
}

static gint
set_vertical_scroll_find_iterator (KTTrackEdit* text, LineParams* lp, void* data)
{
  SetVerticalScrollData *svdata = (SetVerticalScrollData *) data;
  gint return_val;

  if (svdata->last_didnt_wrap)
    svdata->last_line_start = lp->start.index;

  if (svdata->pixel_height <= (gint) text->vadj->value &&
      svdata->pixel_height + LINE_HEIGHT(*lp) > (gint) text->vadj->value)
    {
      svdata->mark = lp->start;

      text->first_cut_pixels = (gint)text->vadj->value - svdata->pixel_height;
      text->first_onscreen_ver_pixel = svdata->pixel_height;
      text->first_line_start_index = svdata->last_line_start;

      return_val = TRUE;
    }
  else
    {
      svdata->pixel_height += LINE_HEIGHT (*lp);

      return_val = FALSE;
    }

  if (!lp->wraps)
    svdata->last_didnt_wrap = TRUE;
  else
    svdata->last_didnt_wrap = FALSE;

  return return_val;
}

static void
scroll_int (KTTrackEdit* text, gint diff)
{
  gfloat upper;

  text->vadj->value += diff;

  upper = text->vadj->upper - text->vadj->page_size;
  text->vadj->value = MIN (text->vadj->value, upper);
  text->vadj->value = MAX (text->vadj->value, 0.0);

  gtk_signal_emit_by_name (GTK_OBJECT (text->vadj), "value_changed");
}

static void 
process_exposes (KTTrackEdit *text)
{
  GdkEvent *event;

  /* Make sure graphics expose events are processed before scrolling
   * again */

  while ((event = gdk_event_get_graphics_expose (text->text_area)) != NULL)
    {
      gtk_widget_event (GTK_WIDGET (text), event);
      if (event->expose.count == 0)
	{
	  gdk_event_free (event);
	  break;
	}
      gdk_event_free (event);
    }
}

static gint last_visible_line_height (KTTrackEdit* text)
{
  GList *cache = text->line_start_cache;
  gint height;

  gdk_window_get_size (text->text_area, NULL, &height);

/*
  for (; cache->next; cache = cache->next)
    if (pixel_height_of(text, cache->next) > height)
      break;
*/

/*
  if (cache)
    return pixel_height_of(text, cache) - 1;
  else
*/
    return 0;
}

static gint first_visible_line_height (KTTrackEdit* text)
{
/*
  if (text->first_cut_pixels)
    return pixel_height_of(text, text->line_start_cache) + 1;
  else
*/
    return 1;
}

static void
scroll_down (KTTrackEdit* text, gint diff0)
{
  GdkRectangle rect;
  gint real_diff = 0;
  gint width, height;

  text->first_onscreen_ver_pixel += diff0;

  while (diff0-- > 0)
    {
      g_assert (text->line_start_cache &&
		text->line_start_cache->next);

      if (text->first_cut_pixels < LINE_HEIGHT(CACHE_DATA(text->line_start_cache)) - 1)
	{
	  text->first_cut_pixels += 1;
	}
      else
	{
	  text->first_cut_pixels = 0;

	  text->line_start_cache = text->line_start_cache->next;

	  text->first_line_start_index =
	    CACHE_DATA(text->line_start_cache).start.index;

		/*
	  if (!text->line_start_cache->next)
	    fetch_lines_forward (text, 1);
		*/
	}

      real_diff += 1;
    }

  gdk_window_get_size (text->text_area, &width, &height);
  if (height > real_diff)
    gdk_draw_pixmap (text->text_area,
		     text->gc,
		     text->text_area,
		     0,
		     real_diff,
		     0,
		     0,
		     width,
		     height - real_diff);

  rect.x      = 0;
  rect.y      = MAX (0, height - real_diff);
  rect.width  = width;
  rect.height = MIN (height, real_diff);

  expose_text (text, &rect, FALSE);
  kt_trackedit_draw_focus ( (GtkWidget *) text);

  if (text->current_line)
    {
      gint cursor_min;

      text->cursor_pos_y -= real_diff;
      cursor_min = drawn_cursor_min(text);

      if (cursor_min < 0)
	{
	  GdkEventButton button;

	  button.x = text->cursor_pos_x;
	  button.y = first_visible_line_height (text);

	  /*mouse_click_1 (text, &button);*/
	}
    }

  if (height > real_diff)
    process_exposes (text);
}

static void
scroll_up (KTTrackEdit* text, gint diff0)
{
  gint real_diff = 0;
  GdkRectangle rect;
  gint width, height;

  text->first_onscreen_ver_pixel += diff0;

  while (diff0++ < 0)
    {
      g_assert (text->line_start_cache);

      if (text->first_cut_pixels > 0)
	{
	  text->first_cut_pixels -= 1;
	}
      else
	{
		/*
	  if (!text->line_start_cache->prev)
	    fetch_lines_backward (text);
		*/

	  text->line_start_cache = text->line_start_cache->prev;

	  text->first_line_start_index =
	    CACHE_DATA(text->line_start_cache).start.index;

	  text->first_cut_pixels = LINE_HEIGHT(CACHE_DATA(text->line_start_cache)) - 1;
	}

      real_diff += 1;
    }

  gdk_window_get_size (text->text_area, &width, &height);
  if (height > real_diff)
    gdk_draw_pixmap (text->text_area,
		     text->gc,
		     text->text_area,
		     0,
		     0,
		     0,
		     real_diff,
		     width,
		     height - real_diff);

  rect.x      = 0;
  rect.y      = 0;
  rect.width  = width;
  rect.height = MIN (height, real_diff);

  expose_text (text, &rect, FALSE);
  kt_trackedit_draw_focus ( (GtkWidget *) text);

  if (text->current_line)
    {
      gint cursor_max;
      gint height;

      text->cursor_pos_y += real_diff;
      cursor_max = drawn_cursor_max(text);
      gdk_window_get_size (text->text_area, NULL, &height);

      if (cursor_max >= height)
	{
	  GdkEventButton button;

	  button.x = text->cursor_pos_x;
	  button.y = last_visible_line_height(text);

	  /*mouse_click_1 (text, &button);*/
	}
    }

  if (height > real_diff)
    process_exposes (text);
}

/**********************************************************************/
/*			      Display Code                            */
/**********************************************************************/

#if 0
/* Assumes mark starts a line.  Calculates the height, width, and
 * displayable character count of a single DISPLAYABLE line.  That
 * means that in line-wrap mode, this does may not compute the
 * properties of an entire line. */
static LineParams
find_line_params (KTTrackEdit* text,
		  const GtkPropertyMark* mark,
		  const PrevTabCont *tab_cont,
		  PrevTabCont *next_cont)
{
  LineParams lp;
  TabStopMark tab_mark = tab_cont->tab_start;
  guint max_display_pixels;
  gchar ch;
  gint ch_width;
  GdkFont *font;

  gdk_window_get_size (text->text_area, (gint*) &max_display_pixels, NULL);
  max_display_pixels -= LINE_WRAP_ROOM;

  lp.wraps             = 0;
  lp.tab_cont          = *tab_cont;
  lp.start             = *mark;
  lp.end               = *mark;
  lp.pixel_width       = tab_cont->pixel_offset;
  lp.displayable_chars = 0;
  lp.font_ascent       = 0;
  lp.font_descent      = 0;

  init_tab_cont (text, next_cont);

  while (!LAST_INDEX(text, lp.end))
    {
      g_assert (lp.end.property);

      ch   = TEXT_INDEX (text, lp.end.index);
      font = MARK_CURRENT_FONT (&lp.end);

      if (ch == LINE_DELIM)
	{
	  /* Newline doesn't count in computation of line height, even
	   * if its in a bigger font than the rest of the line.  Unless,
	   * of course, there are no other characters. */

	  if (!lp.font_ascent && !lp.font_descent)
	    {
	      lp.font_ascent = font->ascent;
	      lp.font_descent = font->descent;
	    }

	  lp.tab_cont_next = *next_cont;

	  return lp;
	}

      ch_width = find_char_width (text, &lp.end, &tab_mark);

      if (ch_width + lp.pixel_width > max_display_pixels)
	{
	  lp.wraps = 1;

	  if (text->line_wrap)
	    {
	      next_cont->tab_start    = tab_mark;
	      next_cont->pixel_offset = 0;

	      if (ch == '\t')
		{
		  /* Here's the tough case, a tab is wrapping. */
		  gint pixels_avail = max_display_pixels - lp.pixel_width;
		  gint space_width  = MARK_CURRENT_TEXT_FONT(&lp.end)->char_widths[' '];
		  gint spaces_avail = pixels_avail / space_width;

		  if (spaces_avail == 0)
		    {
		      /*decrement_mark (&lp.end);*/
		    }
		  else
		    {
		      advance_tab_mark (text, &next_cont->tab_start, '\t');
		      next_cont->pixel_offset = space_width * (tab_mark.to_next_tab -
							       spaces_avail);
		      lp.displayable_chars += 1;
		    }
		}
	      else
		{
		  /* Don't include this character, it will wrap. */
		  /*decrement_mark (&lp.end);*/
		}

	      lp.tab_cont_next = *next_cont;

	      return lp;
	    }
	}
      else
	{
	  lp.displayable_chars += 1;
	}

      lp.font_ascent = MAX (font->ascent, lp.font_ascent);
      lp.font_descent = MAX (font->descent, lp.font_descent);
      lp.pixel_width  += ch_width;

      /*advance_mark(&lp.end);*/
      advance_tab_mark (text, &tab_mark, ch);
    }

  if (LAST_INDEX(text, lp.start))
    {
      /* Special case, empty last line. */
      font = MARK_CURRENT_FONT (&lp.end);

      lp.font_ascent = font->ascent;
      lp.font_descent = font->descent;
    }

  lp.tab_cont_next = *next_cont;

  return lp;
}

static void
expand_scratch_buffer (KTTrackEdit* text, guint len)
{
  if (len >= text->scratch_buffer_len)
    {
      guint i = 1;

      while (i <= len && i < MIN_GAP_SIZE) i <<= 1;

      if (text->scratch_buffer)
	text->scratch_buffer = g_new (guchar, i);
      else
	text->scratch_buffer = g_realloc (text->scratch_buffer, i);

      text->scratch_buffer_len = i;
    }
}
#endif

static void
draw_line (KTTrackEdit* text,
	   gint pixel_start_height,
           int pat,
	   int lineno)
{
  GdkGCValues gc_values;
  GdkFont *font;
  gint fontheight;
  gint i;
  gint len = 0;
  gint xoffset=4;
  guchar* buffer;
  guchar buf[12];
  struct xxm_event *xe;
  /*struct xxm_trackinfo *tinfo = xxp[pat]->info;*/
  struct xxm_trackinfo *tinfo;

  if(!xxp) return;
  if(text->channel >= xxh->chn) return;

  tinfo = xxp[pat]->info;

  font = GTK_WIDGET(text)->style->font;
  fontheight = font->ascent + font->descent;

  xe=&((xxt[tinfo[text->channel].index]->event)[lineno]);

  gdk_draw_text (text->text_area, font,
		 text->gc,
		 xoffset,
		 pixel_start_height,
		 note_strings[xe->note],
		 strlen(note_strings[xe->note]));

  xoffset += gdk_text_width(font, "MMM", 3)+4;
  sprintf(buf, "%02d", xe->ins);
  gdk_draw_text (text->text_area, font,
		 text->gc,
		 xoffset,
		 pixel_start_height,
		 buf,
		 strlen(buf));

  xoffset += gdk_text_width(font, "00", 2)+4;
  sprintf(buf, "%02d", xe->vol);
  gdk_draw_text (text->text_area, font,
		 text->gc,
		 xoffset,
		 pixel_start_height,
		 buf,
		 strlen(buf));

  xoffset += gdk_text_width(font, "00", 2)+4;
  gdk_draw_text (text->text_area, font,
		 text->gc,
		 xoffset,
		 pixel_start_height,
		 fx_strings[xe->fxt],
		 strlen(fx_strings[xe->fxt]));

}

static void
draw_line_wrap (KTTrackEdit* text, guint height /* baseline height */)
{
#if 0
  gint width;
  GdkPixmap *bitmap;
  gint bitmap_width;
  gint bitmap_height;

  if (text->line_wrap)
    {
      bitmap = text->line_wrap_bitmap;
      bitmap_width = line_wrap_width;
      bitmap_height = line_wrap_height;
    }
  else
    {
      bitmap = text->line_arrow_bitmap;
      bitmap_width = line_arrow_width;
      bitmap_height = line_arrow_height;
    }

  gdk_window_get_size (text->text_area, &width, NULL);
  width -= LINE_WRAP_ROOM;

  gdk_gc_set_stipple (text->gc,
		      bitmap);

  gdk_gc_set_fill (text->gc, GDK_STIPPLED);

  gdk_gc_set_foreground (text->gc, &text->widget.style->fg[GTK_STATE_NORMAL]);

  gdk_gc_set_ts_origin (text->gc,
			width + 1,
			height - bitmap_height - 1);

  gdk_draw_rectangle (text->text_area,
		      text->gc,
		      TRUE,
		      width + 1,
		      height - bitmap_height - 1 /* one pixel above the baseline. */,
		      bitmap_width,
		      bitmap_height);

  gdk_gc_set_ts_origin (text->gc, 0, 0);

  gdk_gc_set_fill (text->gc, GDK_SOLID);
#endif
}

static void
undraw_cursor (KTTrackEdit* text, gint absolute)
{
#if 0
  TDEBUG (("in undraw_cursor\n"));

  if (absolute)
    text->cursor_drawn_level = 0;

  if (text->has_cursor && (text->cursor_drawn_level ++ == 0))
    {
      GdkFont* font;

      g_assert(text->cursor_mark.property);

      font = MARK_CURRENT_FONT(&text->cursor_mark);

      if (text->widget.style->bg_pixmap[GTK_STATE_NORMAL])
	{
	  GdkRectangle rect;

	  rect.x = text->cursor_pos_x;
	  rect.y = text->cursor_pos_y - text->cursor_char_offset - font->ascent;
	  rect.width = 1;
	  rect.height = font->ascent + 1; /* @@@ I add one here because draw_line is inclusive, right? */

	  clear_area (text, &rect);
	}
      else
	{
	  if (MARK_CURRENT_BACK (&text->cursor_mark))
	    gdk_gc_set_foreground (text->gc, MARK_CURRENT_BACK (&text->cursor_mark));
	  else
	    gdk_gc_set_foreground (text->gc, &text->widget.style->bg[GTK_STATE_NORMAL]);

	  gdk_draw_line (text->text_area, text->gc, text->cursor_pos_x,
			 text->cursor_pos_y - text->cursor_char_offset, text->cursor_pos_x,
			 text->cursor_pos_y - text->cursor_char_offset - font->ascent);
	}

      if (text->cursor_char)
	{
	  if (font->type == GDK_FONT_FONT)
	    gdk_gc_set_font (text->gc, font);

	  gdk_gc_set_foreground (text->gc, MARK_CURRENT_FORE (&text->cursor_mark));

	  gdk_draw_text (text->text_area, font,
			 text->gc,
			 text->cursor_pos_x,
			 text->cursor_pos_y - text->cursor_char_offset,
			 &text->cursor_char,
			 1);
	}
    }
#endif
}

static gint
drawn_cursor_min (KTTrackEdit* text)
{
#if 0
  if (text->has_cursor)
    {
      GdkFont* font;

      g_assert(text->cursor_mark.property);

      font = MARK_CURRENT_FONT(&text->cursor_mark);

      return text->cursor_pos_y - text->cursor_char_offset - font->ascent;
    }
  else
    return 0;
#endif
}

static gint
drawn_cursor_max (KTTrackEdit* text)
{
#if 0
  if (text->has_cursor)
    {
      GdkFont* font;

      g_assert(text->cursor_mark.property);

      font = MARK_CURRENT_FONT(&text->cursor_mark);

      return text->cursor_pos_y - text->cursor_char_offset;
    }
  else
    return 0;
#endif
}

static void
draw_cursor (KTTrackEdit* text, gint absolute)
{
#if 0
  TDEBUG (("in draw_cursor\n"));

  if (absolute)
    text->cursor_drawn_level = 1;

  if (text->has_cursor && (--text->cursor_drawn_level == 0))
    {
      GdkFont* font;

      g_assert (text->cursor_mark.property);

      font = MARK_CURRENT_FONT (&text->cursor_mark);

      gdk_gc_set_foreground (text->gc, &text->widget.style->fg[GTK_STATE_NORMAL]);

      gdk_draw_line (text->text_area, text->gc, text->cursor_pos_x,
		     text->cursor_pos_y - text->cursor_char_offset,
		     text->cursor_pos_x,
		     text->cursor_pos_y - text->cursor_char_offset - font->ascent);
    }
#endif
}

static void
clear_area (KTTrackEdit *text, GdkRectangle *area)
{
  if (text->widget.style->bg_pixmap[GTK_STATE_NORMAL])
    {
      gint width, height;
      gint x = area->x, y = area->y;
      gint xorig, yorig;

      gdk_window_get_size (text->widget.style->bg_pixmap[GTK_STATE_NORMAL], &width, &height);

      yorig = - text->first_onscreen_ver_pixel;
      xorig = - text->first_onscreen_hor_pixel;

      for (y = area->y; y < area->y + area->height; )
	{
	  gint yoff = (y - yorig) % height;
	  gint yw = MIN(height - yoff, (area->y + area->height) - y);

	  for (x = area->x; x < area->x + area->width; )
	    {
	      gint xoff = (x - xorig) % width;
	      gint xw = MIN(width - xoff, (area->x + area->width) - x);

	      gdk_draw_pixmap (text->text_area,
			       text->gc,
			       text->widget.style->bg_pixmap[GTK_STATE_NORMAL],
			       xoff,
			       yoff,
			       x,
			       y,
			       xw,
			       yw);

	      x += width - xoff;
	    }
	  y += height - yoff;
	}
    }
  else
    gdk_window_clear_area (text->text_area, area->x, area->y, area->width, area->height);
}

void update_pattern (KTTrackEdit *ktt, int pattern)
{
  gint w,h;
  GdkRectangle R;

  gdk_window_get_size(ktt->text_area, &w, &h);
  R.x=0; R.y=0; R.width=w; R.height=h; 
  ktt->current_pattern = pattern;
  ktt->current_row = 0;
  expose_text(ktt, &R, FALSE);
}

void update_row(KTTrackEdit *ktt, int row)
{
  gint w,h;
  GdkRectangle R;

  gdk_window_get_size(ktt->text_area, &w, &h);
  R.x=0; R.y=0; R.width=w; R.height=h; 
  ktt->current_row = row;
  expose_text(ktt, &R, FALSE);
}

static void
expose_text (KTTrackEdit* text, GdkRectangle *area, gboolean cursor)
{
  GdkFont *font;
  gint pixels = 0; /*- text->first_cut_pixels;*/
  gint min_y = area->y;
  gint max_y = area->y + area->height;
  gint width,height;
  gint fontheight;
  gint i;
  gint numlines;

  if(!text) return;

  gdk_window_get_size (text->text_area, &width, &height);
  max_y = MIN (max_y, height);

  TDEBUG (("in expose x=%d y=%d w=%d h=%d\n", area->x, area->y, area->width, area->height));

  clear_area (text, area);

  font = GTK_WIDGET (text)->style->font;
  fontheight = font->ascent + font->descent;
  numlines = height/fontheight;

  gdk_window_set_background (text->text_area, &GTK_WIDGET(text)->style->bg[GTK_STATE_ACTIVE]);
  /*gdk_gc_set_background (text->gc, & GTK_WIDGET(text)->style->bg[GTK_STATE_PRELIGHT]);*/
  /*gdk_draw_rectangle(text->text_area, text->gc, 1, 0, (fontheight*numlines/2)+font->descent-1,width,fontheight+2);*/
  gdk_window_clear_area(text->text_area, 0, (fontheight*numlines/2)-font->descent-1,width,fontheight+2);

  gdk_window_set_background(text->text_area, &GTK_WIDGET(text)->style->white);
  /*gdk_gc_set_background (text->gc, & GTK_WIDGET(text)->style->bg[GTK_STATE_NORMAL]);*/

  if(!xxp) return;

  i = text->current_row - numlines/2 -1;
  if(i<0) {
    pixels = (-i) * fontheight;
    i=0;
  }

  for (; i<xxp[text->current_pattern]->rows && pixels < height; pixels += fontheight,i++)
    {
#if 0
      if (pixels < max_y && (pixels + LINE_HEIGHT(CACHE_DATA(cache))) >= min_y)
	{
#endif
	  draw_line (text, pixels, text->current_pattern, i);

#if 0
	  if (CACHE_DATA(cache).wraps)
	    draw_line_wrap (ktt, pixels + CACHE_DATA(cache).font_ascent);
	}

      if (cursor && ktt->has_cursor && GTK_WIDGET_HAS_FOCUS (&ktt->widget))
	{
	  if (CACHE_DATA(cache).start.index <= ktt->cursor_mark.index &&
	      CACHE_DATA(cache).end.index >= ktt->cursor_mark.index)
	    draw_cursor (ktt, TRUE);
	}

      pixels += LINE_HEIGHT(CACHE_DATA(cache));

      if (!cache->next)
	{
	  fetch_lines_forward (ktt, 1);

	  if (!cache->next)
	    break;
	}
#endif
    }
}

static void
recompute_geometry (KTTrackEdit* text)
{
#if 0
  GtkPropertyMark start_mark;
  gint height;
  gint width;

  free_cache (text);

  /*start_mark = set_vertical_scroll (text);*/

  gdk_window_get_size (text->text_area, &width, &height);

  text->line_start_cache = fetch_lines (text,
					&start_mark,
					NULL,
					FetchLinesPixels,
					height + text->first_cut_pixels);

  /*find_cursor (text);*/
#endif
}

/**********************************************************************/
/*                            Selection                               */
/**********************************************************************/

static gint
kt_trackedit_selection_clear (GtkWidget         *widget,
			  GdkEventSelection *event)
{
#if 0
  GtkEntry *entry;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_ENTRY (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  entry = GTK_ENTRY (widget);

  if (entry->have_selection)
    {
      entry->have_selection = FALSE;
      gtk_entry_queue_draw (entry);
    }
#endif
  return FALSE;
}

static gint
kt_trackedit_selection_request (GtkWidget         *widget,
			    GdkEventSelection *event)
{
#if 0

  GtkEntry *entry;
  gint selection_start_pos;
  gint selection_end_pos;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_ENTRY (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  entry = GTK_ENTRY (widget);
  if (entry->selection_start_pos != entry->selection_end_pos)
    {
      selection_start_pos = MIN (entry->selection_start_pos, entry->selection_end_pos);
      selection_end_pos = MAX (entry->selection_start_pos, entry->selection_end_pos);

      gdk_selection_set (event->requestor, event->selection,
			 event->property, event->time,
			 (guchar*) &entry->text[selection_start_pos],
			 selection_end_pos - selection_start_pos);
    }
#endif
  return FALSE;
}

static gint
kt_trackedit_selection_notify (GtkWidget         *widget,
			   GdkEventSelection *event)
{
#if 0
  GtkEntry *entry;
  gchar *data;
  gint tmp_pos;
  gint reselect;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_ENTRY (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  entry = GTK_ENTRY (widget);

  gdk_selection_get (widget->window, (guchar**) &data);

  reselect = FALSE;
  if (entry->selection_start_pos != entry->selection_end_pos)
    {
      reselect = TRUE;
      gtk_delete_selection (entry);
    }

  tmp_pos = entry->current_pos;
  gtk_entry_insert_text (entry, data, strlen (data), &tmp_pos);

  if (reselect)
    {
      reselect = entry->have_selection;
      gtk_select_region (entry, entry->current_pos, tmp_pos);
      entry->have_selection = reselect;
    }

  entry->current_pos = tmp_pos;

  gtk_entry_queue_draw (entry);
#endif
  return FALSE;
}

/**********************************************************************/
/*                              Debug                                 */
/**********************************************************************/

#ifdef DEBUG_KTTRACKEDIT
static void
kt_trackedit_show_cache_line (KTTrackEdit *text, GList *cache,
			  const char* what, const char* func, gint line)
{
  LineParams *lp = &CACHE_DATA(cache);
  gint i;

  if (cache == text->line_start_cache)
    g_print ("Line Start Cache: ");

  if (cache == text->current_line)
    g_print("Current Line: ");

  g_print ("%s:%d: cache line %s s=%d,e=%d,lh=%d (",
	   func,
	   line,
	   what,
	   lp->start.index,
	   lp->end.index,
	   LINE_HEIGHT(*lp));

  for (i = lp->start.index; i < (lp->end.index + lp->wraps); i += 1)
    g_print ("%c", TEXT_INDEX (text, i));

  g_print (")\n");
}

static void
kt_trackedit_show_cache (KTTrackEdit *text, const char* func, gint line)
{
  GList *l = text->line_start_cache;

  /* back up to the absolute beginning of the line cache */
  while (l->prev)
    l = l->prev;

  g_print ("*** line cache ***\n");
  for (; l; l = l->next)
    kt_trackedit_show_cache_line (text, l, "all", func, line);
}

static void
kt_trackedit_assert_mark (KTTrackEdit         *text,
		      GtkPropertyMark *mark,
		      GtkPropertyMark *before,
		      GtkPropertyMark *after,
		      const gchar     *msg,
		      const gchar     *where,
		      gint             line)
{
  GtkPropertyMark correct_mark = find_mark (text, mark->index);

  if (mark->offset != correct_mark.offset ||
      mark->property != correct_mark.property)
    g_warning ("incorrect %s text property marker in %s:%d, index %d -- bad!", where, msg, line, mark->index);
}

static void
kt_trackedit_assert (KTTrackEdit         *text,
		 const gchar     *msg,
		 gint             line)
{
  GList* cache = text->line_start_cache;
  GtkPropertyMark* before_mark = NULL;
  GtkPropertyMark* after_mark = NULL;

  kt_trackedit_show_props (text, msg, line);

  for (; cache->prev; cache = cache->prev)
    /* nothing */;

  g_print ("*** line markers ***\n");

  for (; cache; cache = cache->next)
    {
      after_mark = &CACHE_DATA(cache).end;
      kt_trackedit_assert_mark (text, &CACHE_DATA(cache).start, before_mark, after_mark, msg, "start", line);
      before_mark = &CACHE_DATA(cache).start;

      if (cache->next)
	after_mark = &CACHE_DATA(cache->next).start;
      else
	after_mark = NULL;

      kt_trackedit_assert_mark (text, &CACHE_DATA(cache).end, before_mark, after_mark, msg, "end", line);
      before_mark = &CACHE_DATA(cache).end;
    }
}

static void
kt_trackedit_show_adj (KTTrackEdit *text,
		   GtkAdjustment *adj,
		   const char* what,
		   const char* func,
		   gint line)
{
  g_print ("*** adjustment ***\n");

  g_print ("%s:%d: %s adjustment l=%.1f u=%.1f v=%.1f si=%.1f pi=%.1f ps=%.1f\n",
	   func,
	   line,
	   what,
	   adj->lower,
	   adj->upper,
	   adj->value,
	   adj->step_increment,
	   adj->page_increment,
	   adj->page_size);
}

static void
kt_trackedit_show_props (KTTrackEdit *text,
		     const char* msg,
		     int line)
{
  GList* props = text->text_properties;
  int proplen = 0;

  g_print ("%s:%d: ", msg, line);

  for (; props; props = props->next)
    {
      TextProperty *p = (TextProperty*)props->data;

      proplen += p->length;

      g_print ("[%d,%p,%p,%p,%p] ", p->length, p, p->font, p->fore_color, p->back_color);
    }

  g_print ("\n");

  if (proplen - 1 != TEXT_LENGTH(text))
    g_warning ("incorrect property list length in %s:%d -- bad!", msg, line);
}
#endif
