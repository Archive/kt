/* Extended Module Player - options.c
 * Copyright (C) 1996,1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */

static char *_id = "$Id$";

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#include <xmp-kt/xmp.h>

extern char *optarg;
static int o, i;
static char *token;

/*extern int probeonly;*/

#define OPT_IGNOREFF	0x100
#define OPT_MODRANGE	0x101
#define OPT_NOPAN	0x102
#define OPT_NOBACK	0x103
#define OPT_NORC	0x104
#define OPT_NTSC	0x105
#define OPT_PROBEONLY	0x106
#define OPT_BIGENDIAN	0x107
#define OPT_LILENDIAN	0x108
#define OPT_STDOUT	0x109
#define OPT_STEREO	0x10a


static void list_wrap (char *s, int l, int r)
{
    int i;
    static int c = 0;
    static int m = 0;
    char *t;

    if (s == NULL) {
	for (i = 0; i < l; i++)
	    printf (" ");
	c = l;
	m = r;
	return;
    } else if (c > l) {
	c++;
	printf (",");
    }

    t = strtok (s, " ");

    while (t) {
	if ((c + strlen (t) + 1) > m) {
	    c = l;
	    printf ("\n");
	    for (i = 0; i < l; i++) {
		printf (" ");
	    }
	} else if (c > l) {
	    printf (" ");
	}
	c += strlen (t) + 1;
	printf ("%s", t);
	t = strtok (NULL, " ");
    }
}


static void usage (char *s, struct xmp_options *opt)
{
    struct xmp_fmt_info *f, *fmt;
    struct xmp_drv_info *d, *drv;
    char **hlp, buf[80];

    printf("Keg Tracker " KT_VERSION "\n");
    printf("Copyright (C) 1998 Peter Ryland and Conrad Parker\n");

    printf ("\nExtended Module Player %s %s\n%s\n",
	__xmp_version, __xmp_date, __xmp_copyright); 

    printf ("\nUsage: %s [options] [modules]\n", s);

    printf ("\nSupported module formats:\n");
    xmp_get_fmt_info (&fmt);
    list_wrap (NULL, 3, 78);
    for (f = fmt; f; f = f->next) {
        sprintf (buf, "%s (%s)", f->suffix, f->tracker);
        list_wrap (buf, 3, 0);
    }
    printf ("\n");

    printf ("\nAvailable drivers:\n");
    xmp_get_drv_info (&drv);
    list_wrap (NULL, 3, 78);
    for (d = drv; d; d = d->next) {
        sprintf (buf, "%s (%s)", d->id, d->description);
        list_wrap (buf, 3, 0);
    }
    printf ("\n");

    for (d = drv; d; d = d->next) {
	if (d->help)
	    printf ("\n%s options:\n", d->description);
	for (hlp = d->help; hlp && *hlp; hlp += 2)
	    printf ("   -D%-20.20s %s\n", hlp[0], hlp[1]);
    }
   
    printf (
"\nPlayer control options:\n"
"   -D parameter[=val]     Pass configuration parameter to the output driver\n"
"   -d --driver name       Force output to the specified device\n"
"   -e --disable-envelope  Disable envelope processing\n"
"   -l --loop              Enable module looping\n"
"   -M --mute ch-list      Mute the specified channels\n"
"   --modrange             Limit the octave range to 3 octaves in MOD files\n"
"   --norc                 Don't read /etc/xmprc or $HOME/.xmprc\n"
"   --ntsc                 Use NTSC timing (60 Hz) instead of PAL (50 Hz)\n"
"   -p --period mode       Period modes (linear or amiga)\n"
"   -S --solo ch-list      Set channels to solo mode\n"
"   -s --start num         Start from the specified order\n"
"   -T --tempo num         Initial tempo (default 6)\n"
"   -t --time num          Maximum playing time in seconds\n"

"\nPlayer sound options:\n"
"   -8 --8bit              Convert 16 bit samples to 8 bit\n"
"   -m --mono              Mono output\n"
"   --nopan                Disable dynamic panning\n"
"   -P --pan pan           Percentual pan amplitude (default %d%%)\n"
"   -r --reverse           Reverse left/right stereo channels\n"
"   --stereo               Stereo output\n"

"\nSoftware mixer options:\n"
"   --big-endian           Use big-endian 16 bit samples\n"
"   -b --bits {8|16}       Software mixer resolution (8 or 16 bits)\n"
"   -c --stdout            Mix the module to stdout\n"
"   -f --frequency rate    Sampling rate in hertz (default %d Hz)\n"
"   -i --interpolate       Enable/disable interpolation (default %s)\n"
"   --little-endian        Use little-endian 16 bit samples\n"
"   -o --output-file name  Mix the module to file ('-' for stdout)\n"
"   -u --unsigned          Set the mixer to use unsigned samples\n"

"\nInformation options:\n"
"   -h --help              Print a summary of the command line options\n"
"   -L --license           Display software license\n"
"   -q --quiet             Quiet mode (verbosity level = 0)\n"
"   -V --version           Print version information\n"
"   -v --verbose           Verbose mode (incremental)\n"
	,opt->mix,
	opt->freq,
	opt->interpolate ? "enabled" : "disabled"
    );
}


void get_options (int argc, char **argv, struct xmp_options *opt)
{
    int optidx = 0;
#define OPTIONS "8Bb:cD:d:f:hiLlM:mo:P:p:qrS:s:T:t:uVv"
    static struct option lopt[] =
    {
	{ "8bit",		 0, 0, '8' },
	{ "big-endian",		 0, 0, OPT_BIGENDIAN },
	{ "bits",		 1, 0, 'b' },
	{ "driver",		 1, 0, 'd' },
	{ "disable-envelope",	 0, 0, 'e' },
	{ "frequency",		 1, 0, 'f' },
	{ "help",		 0, 0, 'h' },
	{ "ignore-ff",		 0, 0, OPT_IGNOREFF },
	{ "interpolate",	 0, 0, 'i' },
	{ "license",		 0, 0, 'L' },
	{ "little-endian",	 0, 0, OPT_LILENDIAN },
	{ "loop",		 0, 0, 'l' },
	{ "mute",		 1, 0, 'M' },
	{ "modrange",		 0, 0, OPT_MODRANGE },
	{ "mono",		 0, 0, 'm' },
	{ "noback",		 0, 0, OPT_NOBACK },
	{ "nopan",		 0, 0, OPT_NOPAN },
	{ "norc",		 0, 0, OPT_NORC },
	{ "ntsc",		 0, 0, OPT_NTSC },
	{ "output-file",	 1, 0, 'o' },
	{ "pan",		 1, 0, 'P' },
	{ "period",		 1, 0, 'p' },
	/*{ "probeonly",		 0, 0, OPT_PROBEONLY },*/
	{ "quiet",		 0, 0, 'q' },
	{ "reverse",		 0, 0, 'r' },
	{ "solo",		 1, 0, 'S' },
	{ "start",		 1, 0, 's' },
	{ "stdout",		 0, 0, 'c' },
	{ "stereo",		 0, 0, OPT_STEREO },
	{ "tempo",		 1, 0, 'T' },
	{ "time",		 1, 0, 't' },
	{ "unsigned",		 0, 0, 'u' },
	{ "version",		 0, 0, 'V' },
	{ "verbose",		 0, 0, 'v' },
	{ NULL,	0, 0, 0 }
    };

    i = 0;
    while ((o = getopt_long (argc, argv, OPTIONS, lopt, &optidx)) != -1) {
	switch (o) {
	case '8':
	    opt->smp8bit = 1;
	    break;
	case 'B':
	    opt->bsmp = 1;
	    break;
	case 'b':
	    opt->res8bit = (atoi (optarg) == 8);
	    break;
	case 'c':
	    opt->outfile = "-";
	    break;
	case 'D':
	    xmp_set_driver_parameter (optarg);
	    break;
	case 'd':
	    opt->drv_id = optarg;
	    break;
	case 'e':
	    opt->noenv = 1;
	    break;
	case 'f':
	    opt->freq = atoi (optarg);
	    break;
	case 'i':
	    opt->interpolate = !opt->interpolate;
	    break;
	case 'L':
	    printf("\nKeg Tracker " KT_VERSION "\n");
	    printf("Copyright (C) 1998 Peter Ryland and Conrad Parker\n");
	    xmp_display_license ();
	    exit (0);
	case 'l':
	    opt->loop = 1;
	    break;
	case OPT_LILENDIAN:
	    opt->bsmp = 0;
	    break;
	case OPT_MODRANGE:
	    opt->modrange = 0;
	    break;
	case 'm':
	    opt->reverse = 0;
	    break;
	case OPT_NOPAN:
	    opt->nopan = 1;
	    break;
	case OPT_NORC:
	    break;
	case OPT_NTSC:
	    opt->ntsc = 1;
	    break;
	case 'o':
	    opt->outfile = optarg;
	    break;
	case 'P':
	    opt->mix = atoi (optarg);
	    if (opt->mix < 0)
		opt->mix = 0;
	    if (opt->mix > 100)
		opt->mix = 100;
	    break;
	case 'p':
	    if (!strcmp (optarg, "amiga"))
		opt->linear = 0;
	    if (!strcmp (optarg, "linear"))
		opt->linear = 1;
	    break;
#if 0
	case OPT_PROBEONLY:
	    probeonly = 1;
	    opt->verbose = 0;
	    break;
#endif
	case 'q':
	    opt->verbose = 0;
	    break;
	case 'r':
	    opt->reverse = -1;
	    break;
	case 'M':
	case 'S':
	    if (o == 'S')
		xmp_channel_mute (0, 32, 1);
	    token = strtok (optarg, ",");
	    while (token) {
		int a, b;
		char buf[40];
		if (strchr (token, '-')) {
		    b = strcspn (token, "-");
		    strncpy (buf, token, b);
		    a = atoi (buf);
		    strncpy (buf, token + b + 1,
			strlen (token) - b - 1);
		    b = atoi (buf);
		} else
		    a = b = atoi (token);
		for (; b >= a; b--) {
		    if (b < 32)
			xmp_channel_mute (b, 1, (o == 'M'));
		}
		token = strtok (NULL, ",");
	    }
	    break;
	case 's':
	    opt->start = atoi (optarg);
	    break;
	case OPT_STEREO:
	    if (!opt->reverse)
		opt->reverse = 1;
	    break;
	case 'T':
	    opt->tempo = atoi (optarg);
	    break;
	case 't':
	    opt->time = atoi (optarg);
	    break;
	case 'u':
	    opt->usmp = 1;
	    break;
	case 'V':
	    printf ("Keg Tracker " KT_VERSION "\n");
	    exit (0);
	case 'v':
	    opt->verbose++;
	    break;
	case 'h':
	    usage (argv[0], opt);
	default:
	    exit (-1);
	}
    }

    /* Set limits */
    if (opt->freq < 1000)
	opt->freq = 1000;	/* Min. rate 1 kHz */
    if (opt->freq > 48000)
	opt->freq = 48000;	/* Max. rate 48 kHz */
}
