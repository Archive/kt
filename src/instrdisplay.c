/*

Keg Tracker.  A GTK-based mod-editor a la fast-tracker.
Copyright (C) 1998 Pete Ryland

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author can be contacted at pdr@pdr.ml.org

*/

static char *_id = "$Id$";

#include <gtk/gtk.h>
#include <xmp-kt/xmp.h>
#include <xmp-kt/xmpi.h>
#include <xmp-kt/xxm.h>
#include "instrdisplay.h"

GtkWidget *instrclist;
GtkWidget *sampleclist;
GtkWidget *songnamewid;

void sampledisplay_load_names(int instr);

void instr_selection_made(GtkWidget *w, gint r, gint c, GdkEventButton *e, gpointer d) {
/*  gtk_clist_unselect_row(GTK_CLIST(w), r, 0);*/
  sampledisplay_load_names(r);
}

void instrdisplay_load_names() {
  guint i = 0;
  gchar *buffer = "";

  for (i = 0; i < xxh->ins; i++) {
    gtk_clist_set_text((GtkCList*)instrclist, i, 1, xxih[i].name);
  }
  for (; i < 128; i++) {
    gtk_clist_set_text((GtkCList*)instrclist, i, 1, buffer);
  }
}

GtkWidget *instrdisplay_new() {
  guint i = 0;
  gchar buffer[3], *buffer2 = "", *bufferptr[2], *titles[2] = {"#", "Instrument Name"};

  instrclist = gtk_clist_new_with_titles(2, titles);
  gtk_clist_column_titles_hide(GTK_CLIST(instrclist));
  gtk_signal_connect(GTK_OBJECT(instrclist), "select_row",
              GTK_SIGNAL_FUNC(instr_selection_made), NULL);
  gtk_clist_set_shadow_type(GTK_CLIST(instrclist), GTK_SHADOW_NONE);
  gtk_clist_set_column_width(GTK_CLIST(instrclist), 0, 15);
  /*gtk_clist_set_column_width(GTK_CLIST(instrclist), 1, 150);*/
  gtk_clist_set_selection_mode(GTK_CLIST(instrclist), GTK_SELECTION_BROWSE);
  /*gtk_clist_set_policy(GTK_CLIST(instrclist), GTK_POLICY_AUTOMATIC,
                                         GTK_POLICY_AUTOMATIC);*/
  
  bufferptr[0] = buffer;
  bufferptr[1] = buffer2;
  for (i = 1; i <= 128; i++) {
    sprintf(buffer, "%02x", i);
    gtk_clist_append((GtkCList*)instrclist, bufferptr);
  }

  return instrclist;
}

void sample_selection_made(GtkWidget *w, gint r, gint c, GdkEventButton *e, gpointer d) {
/*  gtk_clist_unselect_row(GTK_CLIST(w), r, 0);*/
}

void sampledisplay_load_names(int instr) {
  guint i = 0;
  gchar *buffer = "";

  fprintf(stderr, "xxih[%x].nsm = %x\n", instr, xxih[instr].nsm);
  for (i = 0; i < xxih[instr].nsm; i++) {
    gtk_clist_set_text((GtkCList*)sampleclist, i, 1, xxs[xxi[instr][i].sid].name);
  }
  for (; i < 16; i++) {
    gtk_clist_set_text((GtkCList*)sampleclist, i, 1, buffer);
  }
}

GtkWidget *sampledisplay_new() {
  guint i = 0;
  gchar buffer[3], *buffer2 = "", *bufferptr[2], *titles[2] = {"#", "Sample Name"};

  sampleclist = gtk_clist_new_with_titles(2, titles);
  gtk_clist_column_titles_hide(GTK_CLIST(sampleclist));
  gtk_signal_connect(GTK_OBJECT(sampleclist), "select_row",
              GTK_SIGNAL_FUNC(sample_selection_made), NULL);
  gtk_clist_set_shadow_type(GTK_CLIST(sampleclist), GTK_SHADOW_NONE);
  gtk_clist_set_column_width(GTK_CLIST(sampleclist), 0, 15);
  /*gtk_clist_set_column_width(GTK_CLIST(sampleclist), 1, 150);*/
  gtk_clist_set_selection_mode(GTK_CLIST(sampleclist), GTK_SELECTION_BROWSE);
  /*gtk_clist_set_policy(GTK_CLIST(sampleclist), GTK_POLICY_AUTOMATIC,
                                         GTK_POLICY_AUTOMATIC);*/
  
  bufferptr[0] = buffer;
  bufferptr[1] = buffer2;
  for (i = 0; i < 16; i++) {
    sprintf(buffer, "%02x", i);
    gtk_clist_append((GtkCList*)sampleclist, bufferptr);
  }

  return sampleclist;
}

void songname_enter_callback(GtkWidget *widget, GtkWidget *entry) {
  gchar *entry_text;
  entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
  fprintf(stderr, "New name: %s\n", entry_text);
}

extern char *module_name;

void songname_load() {
  /*gtk_entry_set_text((GtkEntry*)songnamewid, xxh->title);*/
  /*gtk_entry_set_text((GtkEntry*)songnamewid, module_name);*/
  gtk_entry_set_text((GtkEntry*)songnamewid, "Blum");
  /*fprintf(stderr, "Name: %s\n", xxh->title);*/
}

GtkWidget *songname_new() {
  songnamewid = gtk_entry_new_with_max_length(31);
  gtk_signal_connect(GTK_OBJECT(songnamewid), "activate",
                     GTK_SIGNAL_FUNC(songname_enter_callback),
                     songnamewid);
  return songnamewid;
}
