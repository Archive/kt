/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 * Modified by Widget.  1998.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __KT_FILESEL_H__
#define __KT_FILESEL_H__


#include <gdk/gdk.h>
#include <gtk/gtkvbox.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define KT_FILE_SELECTION(obj)          GTK_CHECK_CAST (obj, kt_file_selection_get_type (), KtFileSelection)
#define KT_FILE_SELECTION_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, kt_file_selection_get_type (), KtFileSelectionClass)
#define KT_IS_FILE_SELECTION(obj)       GTK_CHECK_TYPE (obj, kt_file_selection_get_type ())


typedef struct _KtFileSelection       KtFileSelection;
typedef struct _KtFileSelectionClass  KtFileSelectionClass;

struct _KtFileSelection
{
  GtkVBox parent;

  GtkWidget *dir_list;
  GtkWidget *file_list;
  GtkWidget *selection_entry;
  GtkWidget *history_pulldown;
  GtkWidget *history_menu;
  GList     *history_list;
  GtkWidget *fileop_dialog;
  GtkWidget *fileop_entry;
  gchar     *fileop_file;
  gpointer   cmpl_state;
  
  GtkWidget *load_button;
  GtkWidget *save_button;
  GtkWidget *fileop_del_file;
  GtkWidget *fileop_ren_file;
  GtkWidget *fileop_c_dir;
  GtkWidget *exit_button;
  
  GtkWidget *action_area;

  void (*load_callback)(gchar *filename);
  void (*save_callback)(gchar *filename);
  void (*exit_callback)(void);
};

struct _KtFileSelectionClass
{
  GtkVBoxClass parent_class;
};

guint      kt_file_selection_get_type     (void);
GtkWidget* kt_file_selection_new          (void);
void       kt_file_selection_set_filename (KtFileSelection        *filesel,
					    const gchar             *filename);
gchar*     kt_file_selection_get_filename (KtFileSelection        *filesel);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __KT_FILESEL_H__ */
