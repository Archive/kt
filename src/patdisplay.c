/*

Keg Tracker.  A GTK-based mod-editor a la fast-tracker.
Copyright (C) 1998 Pete Ryland

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author can be contacted at pdr@pdr.ml.org

*/

static char *_id = "$Id$";

#include <gtk/gtk.h>
#include <xmp-kt/xmp.h>
#include <xmp-kt/xmpi.h>
#include <xmp-kt/xxm.h>
#include "patdisplay.h"

static GtkWidget *clist;

void player_command(GtkWidget *widget, gpointer data);

int real_selection = 1;

static void selection_made(GtkWidget *w, gint r, gint c, GdkEventButton *e, gpointer d) {
/*  gtk_clist_unselect_row(GTK_CLIST(w), r, 0);
  if (gtk_clist_row_is_visible(GTK_CLIST(w), r) != GTK_VISIBILITY_FULL)*/
    gtk_clist_moveto(GTK_CLIST(w), r, c, 0.5, 0.0);
  if (real_selection) player_command(0, (gpointer)('s' + (r << 8)));
}

void patdisplay_reset() {
  int i,t;
  static gchar buffers[4][4];
  static gchar *buffer[2] = {buffers[0], buffers[1]};

  gtk_clist_freeze(GTK_CLIST(clist));
  gtk_clist_clear(GTK_CLIST(clist));
  for(i = 0; i < xxh->len; i++) {
    sprintf(buffer[0], "%02X", i);
    sprintf(buffer[1], "%02X", xxo[i]);
    gtk_clist_append((GtkCList*)clist, (char**)buffer);
  }
  real_selection = 0;
  gtk_clist_select_row(GTK_CLIST(clist), 0, 0);
  real_selection = 1;
  /*gtk_clist_moveto(GTK_CLIST(clist), 0, 0, 0.5, 0.0);*/
  gtk_clist_thaw(GTK_CLIST(clist));
}

void patdisplay_update_pat(int pat) {
  if (pat < 0) return;
  gtk_clist_freeze(GTK_CLIST(clist));
  real_selection = 0;
  gtk_clist_select_row(GTK_CLIST(clist), pat, 0);
  real_selection = 1;
  /*gtk_clist_moveto(GTK_CLIST(clist), pat, 0, 0.5, 0.0);*/
  gtk_clist_thaw(GTK_CLIST(clist));
}

GtkWidget *patdisplay_new() {
  guint           i;
  gchar           *buffer[2] = {"00", "00"};
  gchar           *titles[2] = {"Ord", "Pat"};

  clist = gtk_clist_new_with_titles(2, titles);
  gtk_clist_column_titles_hide(GTK_CLIST(clist));
  gtk_signal_connect(GTK_OBJECT(clist), "select_row",
              GTK_SIGNAL_FUNC(selection_made), NULL);
  gtk_clist_set_shadow_type(GTK_CLIST(clist), GTK_SHADOW_NONE);
  gtk_clist_set_column_width(GTK_CLIST(clist), 0, 15);
  gtk_clist_set_column_width(GTK_CLIST(clist), 1, 15);
  gtk_clist_set_selection_mode(GTK_CLIST(clist), GTK_SELECTION_BROWSE);
  /*gtk_clist_set_policy(GTK_CLIST(clist), GTK_POLICY_ALWAYS,
                                         GTK_POLICY_AUTOMATIC);*/
  /*gtk_widget_set_state(GTK_WIDGET(clist), GTK_STATE_INSENSITIVE);*/
  gtk_widget_show(clist);
  
  gtk_clist_append((GtkCList*)clist, buffer);

  return clist;
}
