/*

Keg Tracker.  A GTK-based mod-editor a la fast-tracker.
Copyright (C) 1998 Pete Ryland

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author can be contacted at pdr@pdr.ml.org

*/

#ifndef __TABLESTUFF_H__
#define __TABLESTUFF_H__

#include <gtk/gtk.h>

#include "pix.h"

typedef enum {
  RegButton, RegButtonG, PixButton, PixButtonG,
  NPThing, RowDisplay, RadioThing, InstrDisplay,
  SampleDisplay, SongName, FileSel, SpinButton,
  Pix, Label, PatDisplay, Slider,
  Stars, CheckButton, CheckButtonG, Scope,
  ToggleButton, ToggleButtonG, PixToggleButton, PixToggleButtonG
} TableElemType;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
} GenericTableElem;

typedef struct {
  int rows, cols, something;
  GenericTableElem **elems;
} TableStuff;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
  void (*callback_func)(GtkWidget *widget, gpointer data);
  gpointer data;
} CommonButtonStuff;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
  void (*callback_func)(GtkWidget *widget, gpointer data);
  gpointer data;
  gchar *label;
} GtkButtonThing;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
  gchar *label;
} GtkLabelWhatsAMe;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
  int channel;
} ScopeDookaDo;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
  void (*callback_func)(GtkWidget *widget, gpointer data);
  gpointer data;
  PixmapThingy *pixmap;
} GtkPixmapButtonThingo;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
  void (*callback_func)(GtkWidget *widget, gpointer data);
  gpointer data;
  PixmapThingy *pixmap;
  GtkWidget *w;
} KtStars;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
  PixmapThingy *pixmap;
} GtkPixmapThingo;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
  /*int num_options;*/
  /*GtkWidget *widgets;*/
  TableStuff **tables;
  GtkWidget **nb;
} GtkNotepadThing;

typedef struct {
  char *label;
  void (*callback_func)(GtkWidget *widget, gpointer data);
  gpointer data;
} RadioThingItem;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
  RadioThingItem **items;
  char *title;
} GtkRadioThing;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
  void (*load_callback_func)(gchar *filename);
  void (*save_callback_func)(gchar *filename);
  void (*exit_callback_func)(GtkWidget *widget, gpointer data);
  gpointer exit_data;
} FileSelThingAMeBob;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
  int min, max, incr;
  GtkAdjustment **sba;
} SpinButtonDoodit;

typedef struct {
  TableElemType tag;
  int left, right, top, bottom;
  char *tooltip;
  int framed;
  int min, max;
  GtkAdjustment **sba;
} SliderBabe;

GtkWidget *setup_table(TableStuff *table);

#endif
