/* 

Keg Tracker.  A GTK-based mod-editor a la fast-tracker.
Copyright (C) 1998 Pete Ryland

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author can be contacted at pdr@pdr.ml.org

*/

static char *_id = "$Id$";

#include <unistd.h>
#include <signal.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <xmp-kt/xmp.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "kt.h"
#include "pix.h"
#include "tablestuff.h"
#include "rowdisplay.h"
#include "tables.h"

extern int optind;

void free_all_shm();
extern int pid;
int control_pid;

extern char *songname;
extern int idle_id;

struct xmp_options opt;

void get_options (int, char **, struct xmp_options *);

void cleanup(int s) {
  fprintf(stderr, "\n*** Blum: signal %d caught [%d] parent:[%d]\n", s, getpid(), getppid());
  free_all_shm();
  if (pid) {
    gtk_idle_remove(idle_id);
    kill(pid, SIGTERM);
    pid = 0;
  }
  gtk_exit(0);
}

void dummy(int s) {
  fprintf(stderr, "\n*** Blum: signal %d ignored\n", s);
  /* do nothing */
}

int player_init();

int argc_g;
char **argv_g;

GtkWidget *window;

int main(int argc, char *argv[]) {
  GtkWidget *main_vbox, *table, *menubar;
  char version[64];
  char gtkrcbuff[256];
  GtkAccelGroup *accels;

  control_pid = getpid();

  gtk_init(&argc, &argv);

  argc_g = argc; argv_g = argv;

  xmp_init(argc, argv, &opt);
  opt.verbose=0;
  get_options(argc, argv, &opt);

  if(argc>optind) songname=argv[optind]; else songname = 0;

  snprintf(gtkrcbuff, 256, "%s/.kt/gtkrc", getenv("HOME"));
  gtk_rc_parse(gtkrcbuff);

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_signal_connect(GTK_OBJECT(window), "destroy", 
                     GTK_SIGNAL_FUNC(quit_callback), 0);
  sprintf(version, "Keg Tracker, version %d.%d.%d", KT_MAJOR_VERSION, KT_MINOR_VERSION, KT_MICRO_VERSION);
  gtk_window_set_title(GTK_WINDOW(window), version);
  gtk_widget_set_usize(GTK_WIDGET(window), 720, 520);

  gtk_widget_show(window);
  setup_pixmaps(window);

  main_vbox = gtk_vbox_new(FALSE, 1);
  gtk_container_border_width(GTK_CONTAINER(main_vbox), 1);
  gtk_container_add(GTK_CONTAINER(window), main_vbox);
  gtk_widget_show(main_vbox);

/*
  get_main_menu(&menubar, &accels);
  gtk_window_add_accel_group(GTK_WINDOW(window), accels);
  gtk_box_pack_start(GTK_BOX(main_vbox), menubar, FALSE, TRUE, 0);
  gtk_widget_show(menubar);
*/

  table = setup_table(&ts);
  gtk_box_pack_start(GTK_BOX(main_vbox), table, TRUE, TRUE, 0);

  signal(SIGTERM, cleanup);
  signal(SIGINT, cleanup);
  signal(SIGQUIT, cleanup);
  signal(SIGFPE, cleanup);
  signal(SIGSEGV, cleanup);
  /*signal(SIGPIPE, dummy);*/
  signal(SIGPIPE, cleanup);

  if(player_init() < 0) return(1);

  gtk_main();

  return(0);
}

void quit_callback(GtkWidget *widget, gpointer data)
{
  free_all_shm();
  if (pid) {
    kill(pid, SIGTERM);
    pid = 0;
  }
  gtk_exit(0);
}
