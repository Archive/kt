/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 * Modified by Widget.  9/1998.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

static char *_id = "$Id$";

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pwd.h>
#include "fnmatch.h"

#include <gdk/gdkkeysyms.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkentry.h>
#include "ktfilesel.h"
#include <gtk/gtkhbox.h>
#include <gtk/gtkhbbox.h>
#include <gtk/gtklabel.h>
#include <gtk/gtklist.h>
#include <gtk/gtklistitem.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtkoptionmenu.h>
#include <gtk/gtkclist.h>
#include <gtk/gtkdialog.h>

#include "filecmpl.h"

#define DIR_LIST_WIDTH   80
#define DIR_LIST_HEIGHT  120
#define FILE_LIST_WIDTH  170
#define FILE_LIST_HEIGHT 120

/* I've put this here so it doesn't get confused with the 
 * file completion interface */
typedef struct _HistoryCallbackArg HistoryCallbackArg;

struct _HistoryCallbackArg
{
  gchar *directory;
  GtkWidget *menu_item;
};

static void kt_file_selection_class_init    (KtFileSelectionClass *klass);
static void kt_file_selection_init          (KtFileSelection      *filesel);
static void kt_file_selection_destroy       (GtkObject             *object);
static gint kt_file_selection_key_press     (GtkWidget             *widget,
					      GdkEventKey           *event,
					      gpointer               user_data);

static void kt_file_selection_file_button (GtkWidget *widget,
					    gint row, 
					    gint column, 
					    GdkEventButton *bevent,
					    gpointer user_data);

static void kt_file_selection_dir_button (GtkWidget *widget,
					   gint row, 
					   gint column, 
					   GdkEventButton *bevent,
					   gpointer data);

static void kt_file_selection_populate      (KtFileSelection      *fs,
					      gchar                 *rel_path,
					      gint                   try_complete);
static void kt_file_selection_abort         (KtFileSelection      *fs);

static void kt_file_selection_update_history_menu (KtFileSelection       *fs,
						    gchar                  *current_dir);

static void kt_file_selection_create_dir (GtkWidget *widget, gpointer data);
static void kt_file_selection_delete_file (GtkWidget *widget, gpointer data);
static void kt_file_selection_rename_file (GtkWidget *widget, gpointer data);
static void kt_file_selection_save_file (GtkWidget *Widget, gpointer data);
static void kt_file_selection_load_file (GtkWidget *Widget, gpointer data);

static GtkVBoxClass *parent_class = NULL;

guint
kt_file_selection_get_type (void)
{
  static guint file_selection_type = 0;

  if (!file_selection_type)
    {
      GtkTypeInfo filesel_info =
      {
	"KtFileSelection",
	sizeof (KtFileSelection),
	sizeof (KtFileSelectionClass),
	(GtkClassInitFunc) kt_file_selection_class_init,
	(GtkObjectInitFunc) kt_file_selection_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      file_selection_type = gtk_type_unique (gtk_vbox_get_type (), &filesel_info);
    }

  return file_selection_type;
}

static void
kt_file_selection_class_init (KtFileSelectionClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  parent_class = gtk_type_class (gtk_vbox_get_type ());

  object_class->destroy = kt_file_selection_destroy;
}

static void
kt_file_selection_init (KtFileSelection *filesel)
{
  GtkWidget *list_hbox;
  GtkWidget *pulldown_hbox;
  
  filesel->cmpl_state = cmpl_init_state ();

  /* hbox for pulldown menu */
  pulldown_hbox = gtk_hbox_new (TRUE, 5);
  gtk_box_pack_start (GTK_BOX (filesel), pulldown_hbox, FALSE, FALSE, 0);
  gtk_widget_show (pulldown_hbox);
  
  /* Pulldown menu */
  filesel->history_pulldown = gtk_option_menu_new ();
  gtk_widget_show (filesel->history_pulldown);
  gtk_box_pack_start (GTK_BOX (pulldown_hbox), filesel->history_pulldown, 
		      FALSE, FALSE, 0);
    
  /*  The horizontal box containing the directory and file listboxes  */
  list_hbox = gtk_hbox_new (FALSE, 5);
  gtk_box_pack_start (GTK_BOX (filesel), list_hbox, TRUE, TRUE, 0);
  gtk_widget_show (list_hbox);

  /* The directories clist */
  filesel->dir_list = gtk_clist_new(1); /*_with_titles (1, dir_title);*/
  gtk_widget_set_usize (filesel->dir_list, DIR_LIST_WIDTH, DIR_LIST_HEIGHT);
  gtk_signal_connect (GTK_OBJECT (filesel->dir_list), "select_row",
		      (GtkSignalFunc) kt_file_selection_dir_button, 
		      (gpointer) filesel);
  /*gtk_clist_set_policy (GTK_CLIST (filesel->dir_list), GTK_POLICY_ALWAYS, GTK_POLICY_AUTOMATIC);*/
  gtk_clist_column_titles_passive (GTK_CLIST (filesel->dir_list));
  gtk_container_border_width (GTK_CONTAINER (filesel->dir_list), 5);
  gtk_box_pack_start (GTK_BOX (list_hbox), filesel->dir_list, TRUE, TRUE, 0);
  gtk_widget_show (filesel->dir_list);

  /* The files clist */
  filesel->file_list = gtk_clist_new(1); /*_with_titles (1, file_title);*/
  gtk_widget_set_usize (filesel->file_list, FILE_LIST_WIDTH, FILE_LIST_HEIGHT);
  gtk_signal_connect (GTK_OBJECT (filesel->file_list), "select_row",
		      (GtkSignalFunc) kt_file_selection_file_button, 
		      (gpointer) filesel);
  /*gtk_clist_set_policy (GTK_CLIST (filesel->file_list), GTK_POLICY_ALWAYS, GTK_POLICY_AUTOMATIC);*/
  gtk_clist_column_titles_passive (GTK_CLIST (filesel->file_list));
  gtk_container_border_width (GTK_CONTAINER (filesel->file_list), 5);
  gtk_box_pack_start (GTK_BOX (list_hbox), filesel->file_list, TRUE, TRUE, 0);
  gtk_widget_show (filesel->file_list);

  /*  The selection entry widget  */
  filesel->selection_entry = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (filesel->selection_entry), "key_press_event",
		      (GtkSignalFunc) kt_file_selection_key_press, filesel);
  gtk_box_pack_start (GTK_BOX (filesel), filesel->selection_entry, TRUE, TRUE, 0);
  gtk_widget_show (filesel->selection_entry);

  /* action area for packing buttons into. */
  filesel->action_area = gtk_hbox_new (TRUE, 0);
  gtk_box_pack_start (GTK_BOX (filesel), filesel->action_area, 
		      FALSE, FALSE, 0);
  gtk_widget_show (filesel->action_area);

  filesel->load_button = gtk_button_new_with_label("Load");
  gtk_box_pack_start (GTK_BOX (filesel->action_area), filesel->load_button, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (filesel->load_button), "clicked",
                      (GtkSignalFunc) kt_file_selection_load_file, 
                      (gpointer) filesel);
  gtk_widget_show(filesel->load_button);

  gtk_signal_connect_object (GTK_OBJECT (filesel->selection_entry), "activate",
                             (GtkSignalFunc) gtk_button_clicked,
                             GTK_OBJECT (filesel->load_button));

  filesel->save_button = gtk_button_new_with_label("Save");
  gtk_box_pack_start(GTK_BOX (filesel->action_area), filesel->save_button, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (filesel->save_button), "clicked",
                      (GtkSignalFunc) kt_file_selection_save_file, 
                      (gpointer) filesel);
  gtk_widget_show (filesel->save_button);

  filesel->fileop_del_file = gtk_button_new_with_label ("Del.");
  gtk_signal_connect (GTK_OBJECT (filesel->fileop_del_file), "clicked",
                      (GtkSignalFunc) kt_file_selection_delete_file, 
                      (gpointer) filesel);
  gtk_box_pack_start (GTK_BOX (filesel->action_area), 
                      filesel->fileop_del_file, TRUE, TRUE, 0);
  gtk_widget_show (filesel->fileop_del_file);

  filesel->fileop_ren_file = gtk_button_new_with_label ("Ren.");
  gtk_signal_connect (GTK_OBJECT (filesel->fileop_ren_file), "clicked",
                      (GtkSignalFunc) kt_file_selection_rename_file, 
                      (gpointer) filesel);
  gtk_box_pack_start (GTK_BOX (filesel->action_area), 
                      filesel->fileop_ren_file, TRUE, TRUE, 0);
  gtk_widget_show (filesel->fileop_ren_file);

  filesel->fileop_c_dir = gtk_button_new_with_label ("Mkdir");
  gtk_signal_connect (GTK_OBJECT (filesel->fileop_c_dir), "clicked",
                      (GtkSignalFunc) kt_file_selection_create_dir, 
                      (gpointer) filesel);
  gtk_box_pack_start (GTK_BOX (filesel->action_area), 
                      filesel->fileop_c_dir, TRUE, TRUE, 0);
  gtk_widget_show (filesel->fileop_c_dir);

  filesel->exit_button = gtk_button_new_with_label ("Close");
  gtk_box_pack_start (GTK_BOX (filesel->action_area), filesel->exit_button, TRUE, TRUE, 0);
  gtk_widget_show (filesel->exit_button);

  if (cmpl_state_okay (filesel->cmpl_state))
    {
      kt_file_selection_populate (filesel, "", FALSE);
    }

  filesel->load_callback = 0;
  filesel->save_callback = 0;

  gtk_widget_grab_focus (filesel->selection_entry);
}

GtkWidget*
kt_file_selection_new (void)
{
  KtFileSelection *filesel;

  filesel = gtk_type_new (kt_file_selection_get_type ());

  return GTK_WIDGET (filesel);
}

void
kt_file_selection_set_filename (KtFileSelection *filesel,
				 const gchar      *filename)
{
  char  buf[MAXPATHLEN];
  const char *name, *last_slash;

  g_return_if_fail (filesel != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (filesel));
  g_return_if_fail (filename != NULL);

  last_slash = strrchr (filename, '/');

  if (!last_slash)
    {
      buf[0] = 0;
      name = filename;
    }
  else
    {
      gint len = MIN (MAXPATHLEN - 1, last_slash - filename + 1);

      strncpy (buf, filename, len);
      buf[len] = 0;

      name = last_slash + 1;
    }

  kt_file_selection_populate (filesel, buf, FALSE);

  if (filesel->selection_entry)
    gtk_entry_set_text (GTK_ENTRY (filesel->selection_entry), name);
}

gchar*
kt_file_selection_get_filename (KtFileSelection *filesel)
{
  static char nothing[2] = "";
  char *text;
  char *filename;

  g_return_val_if_fail (filesel != NULL, nothing);
  g_return_val_if_fail (KT_IS_FILE_SELECTION (filesel), nothing);

  text = gtk_entry_get_text (GTK_ENTRY (filesel->selection_entry));
  if (text)
    {
      filename = cmpl_completion_fullname (text, filesel->cmpl_state);
      return filename;
    }

  return nothing;
}

static void
kt_file_selection_destroy (GtkObject *object)
{
  KtFileSelection *filesel;
  GList *list;
  HistoryCallbackArg *callback_arg;

  g_return_if_fail (object != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (object));

  filesel = KT_FILE_SELECTION (object);
  
  if (filesel->fileop_dialog)
	  gtk_widget_destroy (filesel->fileop_dialog);
  
  if (filesel->history_list)
    {
      list = filesel->history_list;
      while (list)
	{
	  callback_arg = list->data;
	  g_free (callback_arg->directory);
	  g_free (callback_arg);
	  list = list->next;
	}
      g_list_free (filesel->history_list);
      filesel->history_list = NULL;
    }
  
  cmpl_free_state (filesel->cmpl_state);
  filesel->cmpl_state = NULL;

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

/* Begin file operations callbacks */

static void
kt_file_selection_fileop_error (gchar *error_message)
{
  GtkWidget *label;
  GtkWidget *vbox;
  GtkWidget *button;
  GtkWidget *dialog;
  
  g_return_if_fail (error_message != NULL);
  
  /* main dialog */
  dialog = gtk_dialog_new ();
  /*
  gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
		      (GtkSignalFunc) kt_file_selection_fileop_destroy, 
		      (gpointer) fs);
  */
  gtk_window_set_title (GTK_WINDOW (dialog), "Error");
  gtk_window_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);
  
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), 8);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), vbox,
		     FALSE, FALSE, 0);
  gtk_widget_show(vbox);

  label = gtk_label_new(error_message);
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.0);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);
  gtk_widget_show(label);

  /* yes, we free it */
  g_free (error_message);
  
  /* close button */
  button = gtk_button_new_with_label ("Close");
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     (GtkSignalFunc) gtk_widget_destroy, 
			     (gpointer) dialog);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
		     button, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(button);
  gtk_widget_show (button);

  gtk_widget_show (dialog);
}

static void
kt_file_selection_fileop_destroy (GtkWidget *widget, gpointer data)
{
  KtFileSelection *fs = data;

  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));
  
  fs->fileop_dialog = NULL;
}


static void
kt_file_selection_create_dir_confirmed (GtkWidget *widget, gpointer data)
{
  KtFileSelection *fs = data;
  gchar *dirname;
  gchar *path;
  gchar *full_path;
  gchar *buf;
  CompletionState *cmpl_state;
  
  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));

  dirname = gtk_entry_get_text (GTK_ENTRY (fs->fileop_entry));
  cmpl_state = (CompletionState*) fs->cmpl_state;
  path = cmpl_reference_position (cmpl_state);
  
  full_path = g_strconcat (path, "/", dirname, NULL);
  if ( (mkdir (full_path, 0755) < 0) ) 
    {
      buf = g_strconcat ("Error creating directory \"", dirname, "\":  ", 
			 g_strerror(errno), NULL);
      kt_file_selection_fileop_error (buf);
    }
  g_free (full_path);
  
  gtk_widget_destroy (fs->fileop_dialog);
  kt_file_selection_populate (fs, "", FALSE);
}
  
static void
kt_file_selection_create_dir (GtkWidget *widget, gpointer data)
{
  KtFileSelection *fs = data;
  GtkWidget *label;
  GtkWidget *dialog;
  GtkWidget *vbox;
  GtkWidget *button;

  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));

  if (fs->fileop_dialog)
	  return;
  
  /* main dialog */
  fs->fileop_dialog = dialog = gtk_dialog_new ();
  gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
		      (GtkSignalFunc) kt_file_selection_fileop_destroy, 
		      (gpointer) fs);
  gtk_window_set_title (GTK_WINDOW (dialog), "Create Directory");
  gtk_window_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);

  /* If file dialog is grabbed, grab option dialog */
  /* When option dialog is closed, file dialog will be grabbed again */
/*
  if (GTK_WINDOW(fs)->modal)
*/
      gtk_window_set_modal (GTK_WINDOW(dialog), TRUE);

  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), 8);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), vbox,
		     FALSE, FALSE, 0);
  gtk_widget_show(vbox);
  
  label = gtk_label_new("Directory name:");
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.0);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);
  gtk_widget_show(label);

  /*  The directory entry widget  */
  fs->fileop_entry = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (vbox), fs->fileop_entry, 
		      TRUE, TRUE, 5);
  GTK_WIDGET_SET_FLAGS(fs->fileop_entry, GTK_CAN_DEFAULT);
  gtk_widget_show (fs->fileop_entry);
  
  /* buttons */
  button = gtk_button_new_with_label ("Create");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) kt_file_selection_create_dir_confirmed, 
		      (gpointer) fs);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
		     button, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_widget_show(button);
  
  button = gtk_button_new_with_label ("Cancel");
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     (GtkSignalFunc) gtk_widget_destroy, 
			     (gpointer) dialog);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
		     button, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(button);
  gtk_widget_show (button);

  gtk_widget_show (dialog);
}

static void
kt_file_selection_delete_file_confirmed (GtkWidget *widget, gpointer data)
{
  KtFileSelection *fs = data;
  CompletionState *cmpl_state;
  gchar *path;
  gchar *full_path;
  gchar *buf;
  
  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));

  cmpl_state = (CompletionState*) fs->cmpl_state;
  path = cmpl_reference_position (cmpl_state);
  
  full_path = g_strconcat (path, "/", fs->fileop_file, NULL);
  if ( (unlink (full_path) < 0) ) 
    {
      buf = g_strconcat ("Error deleting file \"", fs->fileop_file, "\":  ", 
			 g_strerror(errno), NULL);
      kt_file_selection_fileop_error (buf);
    }
  g_free (full_path);
  
  gtk_widget_destroy (fs->fileop_dialog);
  kt_file_selection_populate (fs, "", FALSE);
}

static void
kt_file_selection_load_file (GtkWidget *Widget, gpointer data)
{
  KtFileSelection *fs = data;
  gchar *filename;
  struct stat st;
  
  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));

  filename = kt_file_selection_get_filename (KT_FILE_SELECTION(fs));
  if (strlen(filename) < 1)
	  return;

  /* return if file does not exist */
  if(stat(filename, &st))
          return;

  /* if it's a directory, go into it */
  if(S_ISDIR(st.st_mode)) {
          kt_file_selection_set_filename(KT_FILE_SELECTION(fs), filename);
          return;
  }

  if (fs->load_callback)
    fs->load_callback(filename);
}

static void
kt_file_selection_save_file (GtkWidget *Widget, gpointer data)
{
  KtFileSelection *fs = data;
  gchar *filename;
  
  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));

  filename = kt_file_selection_get_filename (KT_FILE_SELECTION(fs));
  if (strlen(filename) < 1)
	  return;

  if (fs->save_callback)
    fs->save_callback(filename);
}

static void
kt_file_selection_delete_file (GtkWidget *widget, gpointer data)
{
  KtFileSelection *fs = data;
  GtkWidget *label;
  GtkWidget *vbox;
  GtkWidget *button;
  GtkWidget *dialog;
  gchar *filename;
  gchar *buf;
  
  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));

  if (fs->fileop_dialog)
	  return;

  filename = gtk_entry_get_text (GTK_ENTRY (fs->selection_entry));
  if (strlen(filename) < 1)
	  return;

  fs->fileop_file = filename;
  
  /* main dialog */
  fs->fileop_dialog = dialog = gtk_dialog_new ();
  gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
		      (GtkSignalFunc) kt_file_selection_fileop_destroy, 
		      (gpointer) fs);
  gtk_window_set_title (GTK_WINDOW (dialog), "Delete File");
  gtk_window_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);

  /* If file dialog is grabbed, grab option dialog */
  /* When option dialog is closed, file dialog will be grabbed again */
/*
  if (GTK_WINDOW(fs)->modal)
*/
      gtk_window_set_modal (GTK_WINDOW(dialog), TRUE);
  
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), 8);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), vbox,
		     FALSE, FALSE, 0);
  gtk_widget_show(vbox);

  buf = g_strconcat ("Really delete file \"", filename, "\" ?", NULL);
  label = gtk_label_new(buf);
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.0);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);
  gtk_widget_show(label);
  g_free(buf);
  
  /* buttons */
  button = gtk_button_new_with_label ("Delete");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) kt_file_selection_delete_file_confirmed, 
		      (gpointer) fs);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
		     button, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_widget_show(button);
  
  button = gtk_button_new_with_label ("Cancel");
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     (GtkSignalFunc) gtk_widget_destroy, 
			     (gpointer) dialog);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
		     button, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(button);
  gtk_widget_show (button);

  gtk_widget_show (dialog);

}

static void
kt_file_selection_rename_file_confirmed (GtkWidget *widget, gpointer data)
{
  KtFileSelection *fs = data;
  gchar *buf;
  gchar *file;
  gchar *path;
  gchar *new_filename;
  gchar *old_filename;
  CompletionState *cmpl_state;
  
  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));

  file = gtk_entry_get_text (GTK_ENTRY (fs->fileop_entry));
  cmpl_state = (CompletionState*) fs->cmpl_state;
  path = cmpl_reference_position (cmpl_state);
  
  new_filename = g_strconcat (path, "/", file, NULL);
  old_filename = g_strconcat (path, "/", fs->fileop_file, NULL);

  if ( (rename (old_filename, new_filename)) < 0) 
    {
      buf = g_strconcat ("Error renaming file \"", file, "\":  ", 
			 g_strerror(errno), NULL);
      kt_file_selection_fileop_error (buf);
    }
  g_free (new_filename);
  g_free (old_filename);
  
  gtk_widget_destroy (fs->fileop_dialog);
  kt_file_selection_populate (fs, "", FALSE);
}
  
static void
kt_file_selection_rename_file (GtkWidget *widget, gpointer data)
{
  KtFileSelection *fs = data;
  GtkWidget *label;
  GtkWidget *dialog;
  GtkWidget *vbox;
  GtkWidget *button;
  gchar *buf;
  
  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));

  if (fs->fileop_dialog)
	  return;

  fs->fileop_file = gtk_entry_get_text (GTK_ENTRY (fs->selection_entry));
  if (strlen(fs->fileop_file) < 1)
	  return;
  
  /* main dialog */
  fs->fileop_dialog = dialog = gtk_dialog_new ();
  gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
		      (GtkSignalFunc) kt_file_selection_fileop_destroy, 
		      (gpointer) fs);
  gtk_window_set_title (GTK_WINDOW (dialog), "Rename File");
  gtk_window_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);

  /* If file dialog is grabbed, grab option dialog */
  /* When option dialog  closed, file dialog will be grabbed again */
/*
  if (GTK_WINDOW(fs)->modal)
*/
    gtk_window_set_modal (GTK_WINDOW(dialog), TRUE);
  
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), 8);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), vbox,
		     FALSE, FALSE, 0);
  gtk_widget_show(vbox);
  
  buf = g_strconcat ("Rename file \"", fs->fileop_file, "\" to:", NULL);
  label = gtk_label_new(buf);
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.0);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);
  gtk_widget_show(label);
  g_free(buf);

  /* New filename entry */
  fs->fileop_entry = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (vbox), fs->fileop_entry, 
		      TRUE, TRUE, 5);
  GTK_WIDGET_SET_FLAGS(fs->fileop_entry, GTK_CAN_DEFAULT);
  gtk_widget_show (fs->fileop_entry);
  
  gtk_entry_set_text (GTK_ENTRY (fs->fileop_entry), fs->fileop_file);
  gtk_editable_select_region (GTK_EDITABLE (fs->fileop_entry),
			      0, strlen (fs->fileop_file));

  /* buttons */
  button = gtk_button_new_with_label ("Rename");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) kt_file_selection_rename_file_confirmed, 
		      (gpointer) fs);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
		     button, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_widget_show(button);
  
  button = gtk_button_new_with_label ("Cancel");
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     (GtkSignalFunc) gtk_widget_destroy, 
			     (gpointer) dialog);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
		     button, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(button);
  gtk_widget_show (button);

  gtk_widget_show (dialog);
}


static gint
kt_file_selection_key_press (GtkWidget   *widget,
			      GdkEventKey *event,
			      gpointer     user_data)
{
  KtFileSelection *fs;
  char *text;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  if (event->keyval == GDK_Tab)
    {
      gboolean intercept;

      fs = KT_FILE_SELECTION (user_data);
      text = gtk_entry_get_text (GTK_ENTRY (fs->selection_entry));

      intercept = text && *text;

      text = g_strdup (text);

      kt_file_selection_populate (fs, text, TRUE);

      g_free (text);

      if (intercept)
	gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "key_press_event");

      return intercept;
    }

  return FALSE;
}


static void
kt_file_selection_history_callback (GtkWidget *widget, gpointer data)
{
  KtFileSelection *fs = data;
  HistoryCallbackArg *callback_arg;
  GList *list;

  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));

  list = fs->history_list;
  
  while (list) {
    callback_arg = list->data;
    
    if (callback_arg->menu_item == widget)
      {
	kt_file_selection_populate (fs, callback_arg->directory, FALSE);
	break;
      }
    
    list = list->next;
  }
}

static void 
kt_file_selection_update_history_menu (KtFileSelection *fs,
					gchar *current_directory)
{
  HistoryCallbackArg *callback_arg;
  GtkWidget *menu_item;
  GList *list;
  gchar *current_dir;
  gint dir_len;
  gint i;
  
  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));
  g_return_if_fail (current_directory != NULL);
  
  list = fs->history_list;

  if (fs->history_menu) 
    {
      while (list) {
	callback_arg = list->data;
	g_free (callback_arg->directory);
	g_free (callback_arg);
	list = list->next;
      }
      g_list_free (fs->history_list);
      fs->history_list = NULL;
      
      gtk_widget_destroy (fs->history_menu);
    }
  
  fs->history_menu = gtk_menu_new();

  current_dir = g_strdup (current_directory);

  dir_len = strlen (current_dir);

  for (i = dir_len; i >= 0; i--)
    {
      /* the i == dir_len is to catch the full path for the first 
       * entry. */
      if ( (current_dir[i] == '/') || (i == dir_len))
	{
	  /* another small hack to catch the full path */
	  if (i != dir_len) 
		  current_dir[i + 1] = '\0';
	  menu_item = gtk_menu_item_new_with_label (current_dir);
	  
	  callback_arg = g_new (HistoryCallbackArg, 1);
	  callback_arg->menu_item = menu_item;
	  
	  /* since the autocompletion gets confused if you don't 
	   * supply a trailing '/' on a dir entry, set the full
	   * (current) path to "" which just refreshes the filesel */
	  if (dir_len == i) {
	    callback_arg->directory = g_strdup ("");
	  } else {
	    callback_arg->directory = g_strdup (current_dir);
	  }
	  
	  fs->history_list = g_list_append (fs->history_list, callback_arg);
	  
	  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
			      (GtkSignalFunc) kt_file_selection_history_callback,
			      (gpointer) fs);
	  gtk_menu_append (GTK_MENU (fs->history_menu), menu_item);
	  gtk_widget_show (menu_item);
	}
    }

  gtk_option_menu_set_menu (GTK_OPTION_MENU (fs->history_pulldown), 
			    fs->history_menu);
  g_free (current_dir);
}

static void
kt_file_selection_file_button (GtkWidget *widget,
			       gint row, 
			       gint column, 
			       GdkEventButton *bevent,
			       gpointer user_data)
{
  KtFileSelection *fs = NULL;
  gchar *filename, *temp = NULL;
  
  g_return_if_fail (GTK_IS_CLIST (widget));

  fs = user_data;
  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));
  
  gtk_clist_get_text (GTK_CLIST (fs->file_list), row, 0, &temp);
  filename = g_strdup (temp);

  if (filename)
    {
      if (bevent)
	switch (bevent->type)
	  {
	  case GDK_2BUTTON_PRESS:
	    gtk_button_clicked (GTK_BUTTON (fs->load_button));
	    break;
	    
	  default:
	    gtk_entry_set_text (GTK_ENTRY (fs->selection_entry), filename);
	    break;
	  }
      else
	gtk_entry_set_text (GTK_ENTRY (fs->selection_entry), filename);

      g_free (filename);
    }
}

static void
kt_file_selection_dir_button (GtkWidget *widget,
			       gint row, 
			       gint column, 
			       GdkEventButton *bevent,
			       gpointer user_data)
{
  KtFileSelection *fs = NULL;
  gchar *filename, *temp = NULL;

  g_return_if_fail (GTK_IS_CLIST (widget));

  fs = KT_FILE_SELECTION (user_data);
  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));

  gtk_clist_get_text (GTK_CLIST (fs->dir_list), row, 0, &temp);
  filename = g_strdup (temp);

  if (filename)
    {
      if (bevent)
	switch (bevent->type)
	  {
	  case GDK_2BUTTON_PRESS:
	    kt_file_selection_populate (fs, filename, FALSE);
	    break;
	  
	  default:
	    gtk_entry_set_text (GTK_ENTRY (fs->selection_entry), filename);
	    break;
	  }
      else
	gtk_entry_set_text (GTK_ENTRY (fs->selection_entry), filename);

      g_free (filename);
    }
}

static void
kt_file_selection_populate (KtFileSelection *fs,
			     gchar            *rel_path,
			     gint              try_complete)
{
  CompletionState *cmpl_state;
  PossibleCompletion* poss;
  gchar* filename;
  gint row;
  gchar* rem_path = rel_path;
  gchar* text[2];
  gint did_recurse = FALSE;
  gint possible_count = 0;
  gint selection_index = -1;
  gint file_list_width;
  gint dir_list_width;
  
  g_return_if_fail (fs != NULL);
  g_return_if_fail (KT_IS_FILE_SELECTION (fs));
  
  cmpl_state = (CompletionState*) fs->cmpl_state;
  poss = cmpl_completion_matches (rel_path, &rem_path, cmpl_state);

  if (!cmpl_state_okay (cmpl_state))
    {
      /* Something went wrong. */
      kt_file_selection_abort (fs);
      return;
    }

  g_assert (cmpl_state->reference_dir);

  gtk_clist_freeze (GTK_CLIST (fs->dir_list));
  gtk_clist_clear (GTK_CLIST (fs->dir_list));
  gtk_clist_freeze (GTK_CLIST (fs->file_list));
  gtk_clist_clear (GTK_CLIST (fs->file_list));

  /* Set the dir_list to include ./ and ../ */
  text[1] = NULL;
  text[0] = "./";
  row = gtk_clist_append (GTK_CLIST (fs->dir_list), text);

  text[0] = "../";
  row = gtk_clist_append (GTK_CLIST (fs->dir_list), text);

  /*reset the max widths of the lists*/
  dir_list_width = gdk_string_width(fs->dir_list->style->font,"../");
  gtk_clist_set_column_width(GTK_CLIST(fs->dir_list),0,dir_list_width);
  file_list_width = 1;
  gtk_clist_set_column_width(GTK_CLIST(fs->file_list),0,file_list_width);

  while (poss)
    {
      if (cmpl_is_a_completion (poss))
        {
          possible_count += 1;

          filename = cmpl_this_completion (poss);

	  text[0] = filename;
	  
          if (cmpl_is_directory (poss))
            {
              if (strcmp (filename, "./") != 0 &&
                  strcmp (filename, "../") != 0)
		{
		  int width = gdk_string_width(fs->dir_list->style->font,
					       filename);
		  row = gtk_clist_append (GTK_CLIST (fs->dir_list), text);
		  if(width > dir_list_width)
		    {
		      dir_list_width = width;
		      gtk_clist_set_column_width(GTK_CLIST(fs->dir_list),0,
						 width);
		    }
 		}
	    }
          else
	    {
	      int width = gdk_string_width(fs->file_list->style->font,
				           filename);
	      row = gtk_clist_append (GTK_CLIST (fs->file_list), text);
	      if(width > file_list_width)
	        {
	          file_list_width = width;
	          gtk_clist_set_column_width(GTK_CLIST(fs->file_list),0,
					     width);
	        }
            }
	}

      poss = cmpl_next_completion (cmpl_state);
    }

  gtk_clist_thaw (GTK_CLIST (fs->dir_list));
  gtk_clist_thaw (GTK_CLIST (fs->file_list));

  /* File lists are set. */

  g_assert (cmpl_state->reference_dir);

  if (try_complete)
    {

      /* User is trying to complete filenames, so advance the user's input
       * string to the updated_text, which is the common leading substring
       * of all possible completions, and if its a directory attempt
       * attempt completions in it. */

      if (cmpl_updated_text (cmpl_state)[0])
        {

          if (cmpl_updated_dir (cmpl_state))
            {
	      gchar* dir_name = g_strdup (cmpl_updated_text (cmpl_state));

              did_recurse = TRUE;

              kt_file_selection_populate (fs, dir_name, TRUE);

              g_free (dir_name);
            }
          else
            {
	      if (fs->selection_entry)
		      gtk_entry_set_text (GTK_ENTRY (fs->selection_entry),
					  cmpl_updated_text (cmpl_state));
            }
        }
      else
        {
          selection_index = cmpl_last_valid_char (cmpl_state) -
                            (strlen (rel_path) - strlen (rem_path));
	  if (fs->selection_entry)
	    gtk_entry_set_text (GTK_ENTRY (fs->selection_entry), rem_path);
        }
    }
  else
    {
      if (fs->selection_entry)
	gtk_entry_set_text (GTK_ENTRY (fs->selection_entry), "");
    }

  if (!did_recurse)
    {
      if (fs->selection_entry)
	gtk_entry_set_position (GTK_ENTRY (fs->selection_entry), selection_index);

      if (fs->history_pulldown) 
	{
	  kt_file_selection_update_history_menu (fs, cmpl_reference_position (cmpl_state));
	}
      
    }
}

static void
kt_file_selection_abort(KtFileSelection *fs)
{
  gchar err_buf[256];

  sprintf (err_buf, "Directory unreadable: %s", cmpl_strerror (cmpl_errno));

  /*  BEEP gdk_beep();  */
}
