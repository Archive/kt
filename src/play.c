/* 

Keg Tracker.  A GTK-based mod-editor a la fast-tracker.
Copyright (C) 1998 Pete Ryland

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author can be contacted at pdr@pdr.ml.org

*/

static char *_id = "$Id$";

#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <gtk/gtk.h>

#include <xmp-kt/xmp.h>
#include <xmp-kt/xmpi.h>
#include <xmp-kt/xxm.h>
#include "rowdisplay.h"
#include "instrdisplay.h"

extern struct xmp_options opt;
extern GtkAdjustment *song_length_a, *rep_start_a, *curr_pat_a, *num_rows_a, *add_a, *tempo_a, *bpm_a;

struct ipc_info {
    struct xmp_module_info mi;
    int vol[32];
    int mute[32];
    int progress;
    int pat;
    int row;
    int wresult;
    int pause;
    int cmd;
    int pos;
    int bpm;
    int tpo;
};

int t;
int pid = 0;
extern int control_pid;
struct ipc_info *ii = 0;

static int current_pat = -1, current_row = -1, current_pos = -1, current_bpm = -1, current_tpo = -1;

void player_command(GtkWidget *widget, gpointer data) {
  int cpos, cpat;
  ii->cmd = (int)data;
  switch (ii->cmd & 0xff) {
    case 'f': cpos = current_pos+1; break;
    case 'b': cpos = current_pos-1; break;
    case 's': cpos = ii->cmd >> 8; break;
    default: return;
  }
  gtk_adjustment_set_value(curr_pat_a, cpat = xxo[cpos]);
  gtk_adjustment_set_value(num_rows_a, xxp[cpat]->rows);
  rowdisplay_update_pat(cpat);
  if (!pid) {
    current_pos = cpos;  current_pat = cpat;
    patdisplay_update_pat(current_pos);
  }
}

void mute_channel(GtkWidget *widget, gpointer data) {
  ii->mute[(int)data] = !ii->mute[(int)data];
  ii->cmd = (((int)data) << 8) | 'm';
}

char *songname;

int idle_id;

gint idle_thing() {
  int newrow = ii->row, newpat = ii->pat, newpos = ii->pos, newtpo = ii->tpo, newbpm = ii->bpm;
  usleep(20000);
  if(newpat != current_pat) {
    rowdisplay_update_pat(newpat);
    current_pat = newpat;
    gtk_adjustment_set_value(num_rows_a, xxp[current_pat]->rows);
    gtk_adjustment_set_value(curr_pat_a, current_pat);
  } 
  if(newpos != current_pos) {
    patdisplay_update_pat(newpos);
    current_pos = newpos;
  } 
  if(newrow != current_row) {
    rowdisplay_update_row(current_row, newrow);
    current_row = newrow;
  }
  if(newbpm != current_bpm) {
    gtk_adjustment_set_value(bpm_a, newbpm);
  }
  if(newtpo != current_tpo) {
    gtk_adjustment_set_value(tempo_a, newtpo);
  }
  return TRUE;
}

void play_thread_cleanup(int s) {
  if (s != 15)
    fprintf(stderr, "\n*** Frub: signal %d caught [%d] parent:[%d]\n", s, getpid(), getppid());
  xmp_stop_module();
  /*xmp_close_audio();*/
  _exit(0);
}

void xmp_callback(unsigned long i) {
  unsigned long msg = i >> 4;
  static int pos, pat, row, num_rows;

  if (ii) {
    switch (i & 0xf) {
      case XMP_ECHO_ORD:
        /*printf("%0x", msg);*/
        ii->pos = msg & 0xff;
        if (ii->pos == 0xff) ii->pos = -1;
        ii->pat = pat = msg >> 8;
        break;
      case XMP_ECHO_ROW:
        ii->row = row = msg & 0xff;
        num_rows = msg >> 8;
        break;
      case XMP_ECHO_BPM:
        ii->bpm = msg & 0xff;
        ii->tpo = msg >> 8;
        break;
    }
    switch (ii->cmd & 0xff) {
      case 'f':
        xmp_ord_next();
        ii->cmd = 0;
        break;
      case 'b':
        xmp_ord_prev();
        ii->cmd = 0;
        break;
      case 's':
        /*fprintf(stderr, "Warping to that place known as %02x...\n", (ii->cmd >> 8));*/
        xmp_ord_set(ii->cmd >> 8);
        ii->cmd = 0;
        break;
      case 'm':
        xmp_channel_mute(ii->cmd >> 8, 1, ii->mute[ii->cmd >> 8]);
        ii->cmd = 0;
        break;
    }
  }
}

void stopsng_callback(GtkWidget *widget, gpointer data) {
  if (pid) {
    gtk_idle_remove(idle_id);
    kill(pid, SIGTERM);
    pid = 0;
  }
}

extern int argc_g;
extern char **argv_g;

int player_init() {
  int i;

  /*opt.verbose = 0;*/
  opt.loop = 1;
  if(opt.outfile)
	opt.drv_id = "file";

  xmp_register_event_callback(xmp_callback);
  i=xmp_open_audio(&opt);
  if (i < 0) {
      fprintf (stderr, "%s: ", argv_g[0]);
      switch (i) {
      case XMP_E_DINIT:
          fprintf (stderr, "can't initialize driver\n");
          return -1;
      case XMP_E_NODRV:
          fprintf (stderr, "driver not present\n");
          return -2;
      case XMP_E_DSPEC:
          fprintf (stderr, "driver not specified\n");
          return -3;
      default:
          fprintf (stderr, "unknown error\n");
          return -128;
      }
  }

  if ((ii = xmp_get_shared_mem(sizeof(struct ipc_info))) <= 0) {
      g_print("can't map shared memory\n");
      return;
  }
  ii->wresult = 42;
  xmp_shared_mem_push_mark();
  if(songname) t=xmp_load_module(songname);
  else xmp_mod_new();
  instrdisplay_load_names();
  songname_load();
  rowdisplay_reset();
  patdisplay_reset();
  gtk_adjustment_set_value(song_length_a, 1);
  gtk_adjustment_set_value(rep_start_a, 0);
  gtk_adjustment_set_value(curr_pat_a, 0);
  gtk_adjustment_set_value(num_rows_a, 0x40);
  gtk_adjustment_set_value(add_a, 1);
  gtk_adjustment_set_value(tempo_a, 6);
  gtk_adjustment_set_value(bpm_a, 120);

  return 1;
}

void load_song_callback(char *filename) {

  songname = filename;
  
  if (songname != 0) {
    stopsng_callback(0, 0);
    xmp_shared_mem_pop();
    t = xmp_load_module(songname);
    xmp_get_module_info(&ii->mi);
    ii->pat = 0; ii->row = 0;
    gtk_adjustment_set_value(song_length_a, xxh->len);
    gtk_adjustment_set_value(rep_start_a, xxh->rst);
    gtk_adjustment_set_value(curr_pat_a, xxo[0]);
    gtk_adjustment_set_value(num_rows_a, xxp[xxo[0]]->rows);
    gtk_adjustment_set_value(tempo_a, xxh->tpo);
    gtk_adjustment_set_value(bpm_a, xxh->bpm);
    instrdisplay_load_names();
    songname_load();
    rowdisplay_reset();
    patdisplay_reset();
  }
}

void playpat_callback(GtkWidget *widget, gpointer data) {
  if (pid) {
    gtk_idle_remove(idle_id);
    kill(pid, SIGTERM);
    pid = 0;
  }
  if (ii) {
    if (!(pid = fork())) {
/*
      xmp_tell_parent();
      xmp_wait_parent();
*/
      signal(SIGTERM, play_thread_cleanup);
      signal(SIGINT, play_thread_cleanup);
      signal(SIGQUIT, play_thread_cleanup);
      signal(SIGFPE, play_thread_cleanup);
      signal(SIGSEGV, play_thread_cleanup);
      signal(SIGPIPE, play_thread_cleanup);
      xmp_play_pattern((int)curr_pat_a->value);
      _exit(0);
    } else {
      idle_id = gtk_idle_add((GtkFunction)idle_thing, 0);
    }
  }
}

void playsng_callback(GtkWidget *widget, gpointer data) {
  if (pid) {
    gtk_idle_remove(idle_id);
    kill(pid, SIGTERM);
    pid = 0;
  }
  if (ii) {
    if (!(pid = fork())) {
/*
      xmp_tell_parent();
      xmp_wait_parent();
*/
      signal(SIGTERM, play_thread_cleanup);
      signal(SIGINT, play_thread_cleanup);
      signal(SIGQUIT, play_thread_cleanup);
      signal(SIGFPE, play_thread_cleanup);
      signal(SIGSEGV, play_thread_cleanup);
      signal(SIGPIPE, play_thread_cleanup);
      t = xmp_play_module();
      _exit(0);
    } else {
      idle_id = gtk_idle_add((GtkFunction)idle_thing, 0);
    }
  }
}
