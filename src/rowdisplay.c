/*

Keg Tracker.  A GTK-based mod-editor a la fast-tracker.
Copyright (C) 1998 Pete Ryland

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author can be contacted at pdr@pdr.ml.org

*/

static char *_id = "$Id$";

#include <gtk/gtk.h>
#include <xmp-kt/xmp.h>
#include <xmp-kt/xmpi.h>
#include <xmp-kt/xxm.h>
#include "rowdisplay.h"
#include "rowstrings.h"
#include "kttrackedit.h"

#define DISPLAY_TRACKS 8

GtkWidget *hbox;
GtkWidget *td[DISPLAY_TRACKS];
GtkWidget *te[DISPLAY_TRACKS];

int is_on = 1;
int remember_pat;

static void selection_made(GtkWidget *w, gint r, gint c, GdkEventButton *e, gpointer d) {
#if 0
  gtk_clist_unselect_row(GTK_CLIST(w), r, 0);
/*  if (gtk_clist_row_is_visible(GTK_CLIST(w), r) != GTK_VISIBILITY_FULL)
    gtk_clist_moveto(GTK_CLIST(w), r, c, 0.5, 0.0);
*/
#endif
}

void rowdisplay_reset() {
  int i;

  if (!is_on) { remember_pat = 0; return; }

  for(i=0; i<DISPLAY_TRACKS; i++) 
    update_pattern(KTTRACKEDIT(te[i]), xxo[0]);
}

void rowdisplay_update_pat(int pat) {
  int i;

  if (!is_on) { remember_pat = pat; return; }

  for(i=0; i<DISPLAY_TRACKS; i++)
    update_pattern(KTTRACKEDIT(te[i]), pat);
}

void rowdisplay_update_row(int old_row, int new_row) {
  int i;

  if (!is_on) return;

  for(i=0; i<DISPLAY_TRACKS; i++)
    update_row(KTTRACKEDIT(te[i]), new_row);
}

void rowdisplay_hide() {
  gtk_widget_hide(hbox);
  is_on = 0;
}
void rowdisplay_show() {
  is_on = 1;
  rowdisplay_update_pat(remember_pat);
  gtk_widget_show(hbox);
}

GtkWidget *rowdisplay_new() {
  int           i;

/*
  hadj = gtk_adjustment_new(0.0, 0.0, 16.0, 1.0, 4.0, 1.0),
  vadj = gtk_adjustment_new(0.0, 0.0, 64.0, 1.0, 16.0, 1.0);
*/

  hbox = gtk_hbox_new(TRUE, 0);

  for(i=0; i<DISPLAY_TRACKS; i++) {
    te[i] = kt_trackedit_new(i, NULL, NULL);
    gtk_box_pack_start(GTK_BOX(hbox), te[i], TRUE, TRUE, 0);
    gtk_widget_show(te[i]);
#if 0
    td[i] = trackdisplay_new(i);
    gtk_box_pack_start(GTK_BOX(hbox), td[i], TRUE, TRUE, 0);
    gtk_widget_show(td[i]);
#endif
  }

  return hbox;
}
