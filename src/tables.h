/* 

Keg Tracker.  A GTK-based mod-editor a la fast-tracker.
Copyright (C) 1998 Pete Ryland

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author can be contacted at pdr@pdr.ml.org

*/

#ifndef __TABLES_H__
#define __TABLES_H__

#include <gtk/gtk.h>

#include "pix.h"
#include "tablestuff.h"

void quit_callback(GtkWidget *widget, gpointer data);
void playsng_callback(GtkWidget *widget, gpointer data);
void playpat_callback(GtkWidget *widget, gpointer data);
void stopsng_callback(GtkWidget *widget, gpointer data);
extern GtkMenuEntry menu_items[];
extern int nmenu_items;

/*extern GtkWidget *tlnb;*/
extern TableStuff ts;

#endif
