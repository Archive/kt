/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * KtSpinButton widget for GTK+
 * Copyright (C) 1998 Lars Hamann and Stefan Jeske
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __KT_SPIN_BUTTON_H__
#define __KT_SPIN_BUTTON_H__


#include <gdk/gdk.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkadjustment.h>


#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */


#define KT_TYPE_SPIN_BUTTON                  (kt_spin_button_get_type ())
#define KT_SPIN_BUTTON(obj)                  (GTK_CHECK_CAST ((obj), KT_TYPE_SPIN_BUTTON, KtSpinButton))
#define KT_SPIN_BUTTON_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), KT_TYPE_SPIN_BUTTON, KtSpinButtonClass))
#define KT_IS_SPIN_BUTTON(obj)               (GTK_CHECK_TYPE ((obj), KT_TYPE_SPIN_BUTTON))
#define KT_IS_SPIN_BUTTON_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), KT_TYPE_SPIN_BUTTON))


typedef enum
{
  KT_UPDATE_ALWAYS,
  KT_UPDATE_IF_VALID
} KtSpinButtonUpdatePolicy;

typedef enum
{
  KT_SPIN_STEP_FORWARD,
  KT_SPIN_STEP_BACKWARD,
  KT_SPIN_PAGE_FORWARD,
  KT_SPIN_PAGE_BACKWARD,
  KT_SPIN_HOME,
  KT_SPIN_END,
  KT_SPIN_USER_DEFINED
} KtSpinType;


typedef struct _KtSpinButton	    KtSpinButton;
typedef struct _KtSpinButtonClass  KtSpinButtonClass;


struct _KtSpinButton
{
  GtkEntry entry;
  
  GtkAdjustment *adjustment;
  
  GdkWindow *panel;
  GtkShadowType shadow_type;
  
  guint32 timer;
  guint32 ev_time;
  
  gint climb_rate;
  gint timer_step;
  
  KtSpinButtonUpdatePolicy update_policy;
  
  guint in_child : 2;
  guint click_child : 2;
  guint button : 2;
  guint need_timer : 1;
  guint timer_calls : 3;
  guint digits : 3;
  guint numeric : 1;
  guint wrap : 1;
  guint snap_to_ticks : 1;
};

struct _KtSpinButtonClass
{
  GtkEntryClass parent_class;
};


GtkType		kt_spin_button_get_type	   (void);

void		kt_spin_button_configure	   (KtSpinButton  *spin_button,
						    GtkAdjustment  *adjustment,
						    gint	    climb_rate,
						    guint	    digits);

GtkWidget*	kt_spin_button_new		   (GtkAdjustment  *adjustment,
						    gint	    climb_rate,
						    guint	    digits);

void		kt_spin_button_set_adjustment	   (KtSpinButton  *spin_button,
						    GtkAdjustment  *adjustment);

GtkAdjustment*	kt_spin_button_get_adjustment	   (KtSpinButton  *spin_button);

void		kt_spin_button_set_digits	   (KtSpinButton  *spin_button,
						    guint	    digits);

gfloat		kt_spin_button_get_value_as_float (KtSpinButton  *spin_button);

gint		kt_spin_button_get_value_as_int   (KtSpinButton  *spin_button);

void		kt_spin_button_set_value	   (KtSpinButton  *spin_button, 
						    gint	    value);

void		kt_spin_button_set_update_policy  (KtSpinButton  *spin_button,
						    KtSpinButtonUpdatePolicy  policy);

void		kt_spin_button_set_numeric	   (KtSpinButton  *spin_button,
						    gboolean	    numeric);

void		kt_spin_button_spin		   (KtSpinButton  *spin_button,
						    KtSpinType     direction,
						    gint	    increment);

void		kt_spin_button_set_wrap	   (KtSpinButton  *spin_button,
						    gboolean	    wrap);

void		kt_spin_button_set_shadow_type	   (KtSpinButton  *spin_button,
						    GtkShadowType   shadow_type);

void		kt_spin_button_set_snap_to_ticks  (KtSpinButton  *spin_button,
						    gboolean	    snap_to_ticks);

/* deprecated, defined for backwards compatibility */
#define kt_spin_button_construct kt_spin_button_configure


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __KT_SPIN_BUTTON_H__ */
