/* 

Keg Tracker.  A GTK-based mod-editor a la fast-tracker.
Copyright (C) 1998 Pete Ryland

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author can be contacted at pdr@pdr.ml.org

*/

static char *_id = "$Id$";

#include <stdlib.h>
#include <string.h>
#include <xmp-kt/xmp.h>
#include "tablestuff.h"

void nb_np_callback(GtkWidget *widget, gpointer data);
void default_callback(GtkWidget *widget, gpointer data);
extern GtkWidget *tlnb;

GtkWidget *ffnb;

void ffnb_callback(GtkWidget *widget, gpointer data) {
  gtk_notebook_set_page(GTK_NOTEBOOK(ffnb), (int)data);
}

extern char *songname;

void load_song_callback(gchar *filename);
/*
 {
  songname = calloc(strlen(filename) + 1, 1);
  strcpy(songname, filename);
  default_callback(ffnb, filename);
}
*/

void new_load_song_callback(gchar *filename) {
  load_song_callback(filename);
  gtk_notebook_set_page(GTK_NOTEBOOK(tlnb), 0);
}

void save_song_callback(gchar *filename) {
  xmp_mod_save(filename);
}

/*
GtkButtonThing top_left_diskop_buttons[] = {
  {RegButton, 4, 6, 2, 3, "Load File", GTK_SHADOW_NONE, default_callback, "Load", "Load"},
  {RegButton, 4, 6, 3, 4, "Save File", GTK_SHADOW_NONE, default_callback, "Save", "Save"},
  {RegButton, 4, 6, 4, 5, "Delete File", GTK_SHADOW_NONE, default_callback, "Del.", "Del."},
  {RegButton, 4, 6, 5, 6, "Rename File", GTK_SHADOW_NONE, default_callback, "Ren", "Ren"},
  {RegButton, 4, 6, 6, 7, "Make Directory", GTK_SHADOW_NONE, default_callback, "Mkdir", "Mkdir"},
  {RegButton, 4, 6, 7, 8, "Close File Panel", GTK_SHADOW_NONE, nb_np_callback, &tlnb, "Close"}
};
*/

RadioThingItem file_type_radio_thing_items_array[] = {
  {"Module", ffnb_callback, (gpointer)0},
  {"Instr", ffnb_callback, (gpointer)1},
  {"Sample", ffnb_callback, (gpointer)2},
  {"Pattern", ffnb_callback, (gpointer)3},
  {"Track", ffnb_callback, (gpointer)4}
};

RadioThingItem *file_type_radio_items[] = {
  &file_type_radio_thing_items_array[0],
  &file_type_radio_thing_items_array[1],
  &file_type_radio_thing_items_array[2],
  &file_type_radio_thing_items_array[3],
  &file_type_radio_thing_items_array[4],
  0
};

GtkRadioThing file_type_radio_thing = {
  RadioThing, 0, 4, 0, 6, "File Type", GTK_SHADOW_NONE, file_type_radio_items, 0
};

RadioThingItem module_file_format_radio_thing_items_array[] = {
  {"MOD", default_callback, "MOD"},
  {"XM", default_callback, "XM"}
  /*{"WAV", default_callback, "WAV"}*/
};

RadioThingItem *module_file_format_radio_items[] = {
  &module_file_format_radio_thing_items_array[0],
  &module_file_format_radio_thing_items_array[1],
  /*&module_file_format_radio_thing_items_array[2],*/
  0
};

GtkRadioThing module_file_format_radio_thing = {
  RadioThing, 0, 4, 0, 4, "Save Format", GTK_SHADOW_NONE, module_file_format_radio_items, "Save As:"
};

GenericTableElem *module_file_format_table_elems[] = {
  (GenericTableElem*)&module_file_format_radio_thing,
  0
};

TableStuff module_file_format_ts = {
  4, 4, TRUE, module_file_format_table_elems
};

RadioThingItem instr_file_format_radio_thing_items_array[] = {
  {"XI", default_callback, "XI"}
};

RadioThingItem *instr_file_format_radio_items[] = {
  &instr_file_format_radio_thing_items_array[0],
  0
};

GtkRadioThing instr_file_format_radio_thing = {
  RadioThing, 0, 4, 0, 4, "Save Format", GTK_SHADOW_NONE, instr_file_format_radio_items, "Save As:"
};

GenericTableElem *instr_file_format_table_elems[] = {
  (GenericTableElem*)&instr_file_format_radio_thing,
  0
};

TableStuff instr_file_format_ts = {
  4, 4, TRUE, instr_file_format_table_elems
};

RadioThingItem sample_file_format_radio_thing_items_array[] = {
  {"SMP", default_callback, "SMP"},
  /*{"IFF", default_callback, "IFF"},*/
  {"WAV", default_callback, "WAV"}
};

RadioThingItem *sample_file_format_radio_items[] = {
  &sample_file_format_radio_thing_items_array[0],
  &sample_file_format_radio_thing_items_array[1],
  /*&sample_file_format_radio_thing_items_array[2],*/
  0
};

GtkRadioThing sample_file_format_radio_thing = {
  RadioThing, 0, 4, 0, 4, "Save Format", GTK_SHADOW_NONE, sample_file_format_radio_items, "Save As:"
};

GenericTableElem *sample_file_format_table_elems[] = {
  (GenericTableElem*)&sample_file_format_radio_thing,
  0
};

TableStuff sample_file_format_ts = {
  4, 4, TRUE, sample_file_format_table_elems
};

RadioThingItem pattern_file_format_radio_thing_items_array[] = {
  {"XP", default_callback, "XP"}
};

RadioThingItem *pattern_file_format_radio_items[] = {
  &pattern_file_format_radio_thing_items_array[0],
  0
};

GtkRadioThing pattern_file_format_radio_thing = {
  RadioThing, 0, 4, 0, 4, "Save Format", GTK_SHADOW_NONE, pattern_file_format_radio_items, "Save As:"
};

GenericTableElem *pattern_file_format_table_elems[] = {
  (GenericTableElem*)&pattern_file_format_radio_thing,
  0
};

TableStuff pattern_file_format_ts = {
  4, 4, TRUE, pattern_file_format_table_elems
};

RadioThingItem track_file_format_radio_thing_items_array[] = {
  {"XT", default_callback, "XT"}
};

RadioThingItem *track_file_format_radio_items[] = {
  &track_file_format_radio_thing_items_array[0],
  0
};

GtkRadioThing track_file_format_radio_thing = {
  RadioThing, 0, 4, 0, 4, "Save Format", GTK_SHADOW_NONE, track_file_format_radio_items, "Save As:"
};

GenericTableElem *track_file_format_table_elems[] = {
  (GenericTableElem*)&track_file_format_radio_thing,
  0
};

TableStuff track_file_format_ts = {
  4, 4, TRUE, track_file_format_table_elems
};

TableStuff *file_format_ts[] = {
  &module_file_format_ts,
  &instr_file_format_ts,
  &sample_file_format_ts,
  &pattern_file_format_ts,
  &track_file_format_ts,
  0
};

GtkNotepadThing file_format_np = {
  NPThing, 0, 4, 6, 10, 0, GTK_SHADOW_NONE, file_format_ts, &ffnb
};

FileSelThingAMeBob filesel = {
  FileSel, 4, 20, 0, 10, 0, GTK_SHADOW_NONE,
  new_load_song_callback,
  save_song_callback, 
  nb_np_callback, &tlnb, 
};

GenericTableElem *top_left_diskop_table_elems[] = {
/*
  (GenericTableElem*)&top_left_diskop_buttons[0],
  (GenericTableElem*)&top_left_diskop_buttons[1],
  (GenericTableElem*)&top_left_diskop_buttons[2],
  (GenericTableElem*)&top_left_diskop_buttons[3],
  (GenericTableElem*)&top_left_diskop_buttons[4],
  (GenericTableElem*)&top_left_diskop_buttons[5],
*/
  (GenericTableElem*)&file_type_radio_thing,
  (GenericTableElem*)&file_format_np,
  (GenericTableElem*)&filesel,
  0
};

TableStuff top_left_diskop_ts = {
  10, 20, TRUE, top_left_diskop_table_elems
};
