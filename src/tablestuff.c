/*

Keg Tracker.  A GTK-based mod-editor a la fast-tracker.
Copyright (C) 1998 Pete Ryland

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author can be contacted at pdr@pdr.ml.org

*/

static char *_id = "$Id$";

#include <stdio.h>
#include "tablestuff.h"
#include "rowdisplay.h"
#include "patdisplay.h"
#include "instrdisplay.h"
#include "ktfilesel.h"
#include "ktspinbutton.h"
#include "stars.h"

void mute_channel(GtkWidget *widget, gpointer data);

GtkWidget *setup_table(TableStuff *table) {
  GenericTableElem **p = table->elems;
  GtkWidget *new_widget, *t, *t2, *pixmapwid;
  GSList *group;
  CommonButtonStuff *cbs;
  TableStuff **ts;
  RadioThingItem **rti;
  int framed;
  GtkWidget *frame;
  SpinButtonDoodit *ksb;
  SliderBabe *sb;
  ScopeDookaDo *sdd;
  GtkTooltips *tooltips = gtk_tooltips_new();
  char *tooltip;

  GtkWidget *tablewid = gtk_table_new(table->rows, table->cols, table->something);

  while (*p) {
    /*fprintf(stderr, "%p: %02x\t", *p, (*p)->tag);*/
    switch ((*p)->tag) {
      case Label:
        new_widget = gtk_label_new(((GtkLabelWhatsAMe*)*p)->label);
        gtk_label_set_justify((GtkLabel*)new_widget, GTK_JUSTIFY_LEFT);
        break;
      case ToggleButton:
        new_widget = gtk_toggle_button_new_with_label(((GtkButtonThing*)*p)->label);
        goto common_button_stuff;
      case ToggleButtonG:
        new_widget = gtk_toggle_button_new_with_label(((GtkButtonThing*)*p)->label);
        gtk_widget_set_state(new_widget, GTK_STATE_INSENSITIVE);
        goto common_button_stuff;
      case PixToggleButtonG: 
        pixmapwid = gtk_pixmap_new(((GtkPixmapButtonThingo*)*p)->pixmap->pixmap,
                                   ((GtkPixmapButtonThingo*)*p)->pixmap->mask);
        gtk_widget_show(pixmapwid);
        new_widget = gtk_toggle_button_new();
        gtk_widget_set_state(new_widget, GTK_STATE_INSENSITIVE);
        gtk_container_add(GTK_CONTAINER(new_widget), pixmapwid);
        goto common_button_stuff;
      case PixToggleButton: 
        pixmapwid = gtk_pixmap_new(((GtkPixmapButtonThingo*)*p)->pixmap->pixmap,
                                   ((GtkPixmapButtonThingo*)*p)->pixmap->mask);
        gtk_widget_show(pixmapwid);
        new_widget = gtk_toggle_button_new();
        gtk_container_add(GTK_CONTAINER(new_widget), pixmapwid);
        goto common_button_stuff;
      case RegButton:
        new_widget = gtk_button_new_with_label(((GtkButtonThing*)*p)->label);
        goto common_button_stuff;
      case RegButtonG:
        new_widget = gtk_button_new_with_label(((GtkButtonThing*)*p)->label);
        gtk_widget_set_state(new_widget, GTK_STATE_INSENSITIVE);
        goto common_button_stuff;
      case Pix:
        new_widget = gtk_pixmap_new(((GtkPixmapThingo*)*p)->pixmap->pixmap,
                                   ((GtkPixmapThingo*)*p)->pixmap->mask);
        break;
      case CheckButtonG: 
        new_widget = gtk_check_button_new_with_label(((GtkButtonThing*)*p)->label);
        gtk_widget_set_state(new_widget, GTK_STATE_INSENSITIVE);
        goto common_button_stuff;
      case CheckButton: 
        new_widget = gtk_check_button_new_with_label(((GtkButtonThing*)*p)->label);
        goto common_button_stuff;
      case Scope: 
        new_widget = gtk_toggle_button_new();
        gtk_signal_connect(GTK_OBJECT(new_widget), "clicked",
             GTK_SIGNAL_FUNC(mute_channel), (gpointer)(((ScopeDookaDo*)*p)->channel));
        GTK_WIDGET_UNSET_FLAGS(new_widget, GTK_CAN_FOCUS);
        break;
      case PixButtonG: 
        pixmapwid = gtk_pixmap_new(((GtkPixmapButtonThingo*)*p)->pixmap->pixmap,
                                   ((GtkPixmapButtonThingo*)*p)->pixmap->mask);
        gtk_widget_show(pixmapwid);
        new_widget = gtk_button_new();
        gtk_widget_set_state(new_widget, GTK_STATE_INSENSITIVE);
        gtk_container_add(GTK_CONTAINER(new_widget), pixmapwid);
        goto common_button_stuff;
      case PixButton: 
        pixmapwid = gtk_pixmap_new(((GtkPixmapButtonThingo*)*p)->pixmap->pixmap,
                                   ((GtkPixmapButtonThingo*)*p)->pixmap->mask);
        gtk_widget_show(pixmapwid);
        new_widget = gtk_button_new();
        gtk_container_add(GTK_CONTAINER(new_widget), pixmapwid);
      common_button_stuff:
        cbs = (CommonButtonStuff*)*p;
        gtk_signal_connect(GTK_OBJECT(new_widget), "clicked",
             GTK_SIGNAL_FUNC(cbs->callback_func), cbs->data);
        GTK_WIDGET_UNSET_FLAGS(new_widget, GTK_CAN_FOCUS);
        break;
      case NPThing:
        *(((GtkNotepadThing*)*p)->nb) = new_widget = gtk_notebook_new();
        gtk_notebook_set_show_tabs(GTK_NOTEBOOK(new_widget), FALSE);
        gtk_notebook_set_show_border(GTK_NOTEBOOK(new_widget), FALSE);
        for (ts = ((GtkNotepadThing*)*p)->tables; *ts; ts++) {
          t = setup_table(*ts);
          gtk_widget_show(t);
          gtk_notebook_append_page(GTK_NOTEBOOK(new_widget), t, gtk_label_new("Tab"));
        }
        GTK_WIDGET_UNSET_FLAGS(new_widget, GTK_CAN_FOCUS);
        break;
      case RowDisplay:
        new_widget = rowdisplay_new();
        /*new_widget = gtk_button_new_with_label("Rowdisplay");*/
        break;
      case PatDisplay:
        new_widget = patdisplay_new();
        break;
      case InstrDisplay:
        new_widget = instrdisplay_new();
        break;
      case SampleDisplay:
        new_widget = sampledisplay_new();
        break;
      case SongName:
        new_widget = songname_new();
        break;
      case Stars:
        new_widget = gtk_event_box_new();
        t = kt_stars_new();
        kt_stars_set_logo_pixmp(t, ((KtStars*)*p)->pixmap->pixmap,
                                   ((KtStars*)*p)->pixmap->mask);
        gtk_container_add(GTK_CONTAINER(new_widget), t);
        gtk_widget_show(t);
        ((KtStars*)*p)->w = t;
        gtk_widget_set_events(new_widget, GDK_BUTTON_PRESS_MASK);
        gtk_signal_connect(GTK_OBJECT(new_widget), "button_press_event",
             GTK_SIGNAL_FUNC(((KtStars*)*p)->callback_func), ((KtStars*)*p)->data);
        GTK_WIDGET_UNSET_FLAGS(new_widget, GTK_CAN_FOCUS);
        break;
      case FileSel:
        new_widget = kt_file_selection_new();
        ((KtFileSelection*)new_widget)->load_callback = ((FileSelThingAMeBob*)(*p))->load_callback_func;
        ((KtFileSelection*)new_widget)->save_callback = ((FileSelThingAMeBob*)(*p))->save_callback_func;
        gtk_signal_connect(GTK_OBJECT(((KtFileSelection*)new_widget)->exit_button),
               "clicked",
               (GtkSignalFunc)((FileSelThingAMeBob*)(*p))->exit_callback_func,
               (gpointer)((FileSelThingAMeBob*)(*p))->exit_data); 
        break;
      case RadioThing:
        new_widget = gtk_vbox_new(FALSE, 0);
        gtk_container_border_width(GTK_CONTAINER(new_widget), 0);
        if (((GtkRadioThing*)*p)->title) {
          t = gtk_label_new(((GtkRadioThing*)*p)->title);
          gtk_box_pack_start(GTK_BOX(new_widget), t, FALSE, FALSE, 0);
          gtk_widget_show(t);
        }
        rti = ((GtkRadioThing*)*p)->items;
        group = 0; t2 = 0;
        while(*rti) {
          t = gtk_radio_button_new_with_label(group, (*rti)->label);
          if (!t2)
            t2 = t;
          gtk_box_pack_start(GTK_BOX(new_widget), t, FALSE, FALSE, 0);
          gtk_widget_show(t);
          gtk_signal_connect(GTK_OBJECT(t), "clicked", 
             GTK_SIGNAL_FUNC((*rti)->callback_func), (*rti)->data);
          rti++;
          if (*rti)
            group = gtk_radio_button_group(GTK_RADIO_BUTTON(t));
        }
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(t2), TRUE);
        GTK_WIDGET_UNSET_FLAGS(new_widget, GTK_CAN_FOCUS);
        break;
      case Slider: 
        sb = (SliderBabe*)*p;
        (*(sb->sba)) = (GtkAdjustment*)gtk_adjustment_new(
                                sb->min, sb->min, sb->max, 1, 1, 1);
        new_widget = gtk_vscale_new(*(sb->sba));
        gtk_scale_set_digits((GtkScale*)new_widget, 0);
        GTK_WIDGET_UNSET_FLAGS(new_widget, GTK_CAN_FOCUS);
        break;
      case SpinButton: 
        ksb = (SpinButtonDoodit*)*p;
        (*(ksb->sba)) = (GtkAdjustment*)gtk_adjustment_new(
                                ksb->min, ksb->min, ksb->max, ksb->incr, ksb->incr << 4, ksb->incr << 4);
        new_widget = kt_spin_button_new(*(ksb->sba), ksb->incr, 2);
        break;
      default:
        g_print("Error in tablestuff.c: setup_table(): Unknown TableElem tag.\n");
        gtk_exit(1);
    }
    if ((tooltip = (*p)->tooltip) != 0) {
      gtk_tooltips_set_tip(tooltips, new_widget, tooltip, NULL);
    }
    if ((framed = (*p)->framed) != 0) {
      frame = gtk_frame_new(0);
      gtk_container_border_width(GTK_CONTAINER(frame), 1);
      gtk_frame_set_shadow_type((GtkFrame*)frame, framed);
      gtk_container_add(GTK_CONTAINER(frame), new_widget);
      gtk_widget_show(new_widget);
      new_widget = frame;
    }
    gtk_table_attach_defaults(GTK_TABLE(tablewid), new_widget,
            (*p)->left, (*p)->right, (*p)->top, (*p)->bottom);
    gtk_widget_show(new_widget);
    p++;
  }
  gtk_container_set_resize_mode(GTK_CONTAINER(tablewid), GTK_RESIZE_IMMEDIATE);
  gtk_widget_show(tablewid);
  return tablewid;
}
