/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __KTTRACKEDIT_H__
#define __KTTRACKEDIT_H__


#include <gdk/gdk.h>
#include <gtk/gtkadjustment.h>
#include <gtk/gtkwidget.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define KTTRACKEDIT(obj)          GTK_CHECK_CAST (obj, kt_trackedit_get_type (), KTTrackEdit)
#define KTTRACKEDIT_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, kt_trackedit_get_type (), KTTrackEditClass)
#define IS_KTTRACKEDIT(obj)       GTK_CHECK_TYPE (obj, kt_trackedit_get_type ())


/*
typedef struct _GtkPropertyMark   GtkPropertyMark;
*/
typedef struct _KTTrackEdit           KTTrackEdit;
typedef struct _KTTrackEditClass      KTTrackEditClass;

typedef void  (*KTTrackEditFunction) (KTTrackEdit *text);

struct _KTTrackEdit
{
  GtkWidget widget;

  GdkWindow *text_area;

  GtkAdjustment *hadj;
  GtkAdjustment *vadj;

  GdkGC *gc;

  int channel;
  int current_pattern;
  int current_row;

  GdkPixmap* line_wrap_bitmap;
  GdkPixmap* line_arrow_bitmap;

		      /* GAPPED TEXT SEGMENT */

  /* The text, a single segment of text a'la emacs, with a gap
   * where insertion occurs. */
  guchar* text;
  /* The allocated length of the text segment. */
  guint text_len;
  /* The gap position, index into address where a char
   * should be inserted. */
  guint gap_position;
  /* The gap size, s.t. *(text + gap_position + gap_size) is
   * the first valid character following the gap. */
  guint gap_size;
  /* The last character position, index into address where a
   * character should be appeneded.  Thus, text_end - gap_size
   * is the length of the actual data. */
  guint text_end;
			/* LINE START CACHE */

  /* A cache of line-start information.  Data is a LineParam*. */
  GList *line_start_cache;
  /* Index to the start of the first visible line. */
  guint first_line_start_index;
  /* The number of pixels cut off of the top line. */
  guint first_cut_pixels;
  /* First visible horizontal pixel. */
  guint first_onscreen_hor_pixel;
  /* First visible vertical pixel. */
  guint first_onscreen_ver_pixel;

			     /* FLAGS */

  /* True iff the cursor has been placed yet. */
  guint has_cursor : 1;
  /* True iff this buffer is editable. (Allowing a cursor to be placed). */
  guint is_editable : 1;
  /* True iff this buffer is wrapping lines, otherwise it is using a
   * horizontal scrollbar. */
  guint line_wrap : 1;
  /* Frozen, don't do updates. @@@ fixme */
  guint freeze : 1;
  /* Whether a selection. */
  guint has_selection : 1;
  /* Whether the selection is in the clipboard. */
  guint own_selection : 1;
  /* Whether it has been realized yet. */

			/* TEXT PROPERTIES */

  /* A doubly-linked-list containing TextProperty objects. */
  GList *text_properties;
  /* The end of this list. */
  GList *text_properties_end;
  /* The first node before or on the point along with its offset to
   * the point and the buffer's current point.  This is the only
   * PropertyMark whose index is guaranteed to remain correct
   * following a buffer insertion or deletion. */
  GtkPropertyMark point;

			  /* SCRATCH AREA */

  guchar* scratch_buffer;
  guint   scratch_buffer_len;

			   /* SCROLLING */

  gint last_ver_value;

			     /* CURSOR */

  gint            cursor_pos_x;       /* Position of cursor. */
  gint            cursor_pos_y;       /* Baseline of line cursor is drawn on. */
  GtkPropertyMark cursor_mark;        /* Where it is in the buffer. */
  gchar           cursor_char;        /* Character to redraw. */
  gchar           cursor_char_offset; /* Distance from baseline of the font. */
  gint            cursor_virtual_x;   /* Where it would be if it could be. */
  gint            cursor_drawn_level; /* How many people have undrawn. */

			  /* Current Line */

  GList *current_line;

			   /* Tab Stops */

  GList *tab_stops;
  gint default_tab_width;

			  /* Key bindings */

  KTTrackEditFunction control_keys[26];
  KTTrackEditFunction alt_keys[26];

		      /* Selection nonsense. */

  guint selection_start;
  guint selection_stop;
};

struct _KTTrackEditClass
{
  GtkWidgetClass parent_class;
};


guint      kt_trackedit_get_type        (void);
GtkWidget* kt_trackedit_new             (int channel, GtkAdjustment *hadj,
				     GtkAdjustment *vadj);
void update_pattern (KTTrackEdit *ktt, int pattern);
void update_pattern (KTTrackEdit *ktt, int row);
void       kt_trackedit_set_editable    (KTTrackEdit       *text,
				     gint           editable);
void       kt_trackedit_set_adjustments (KTTrackEdit       *text,
				     GtkAdjustment *hadj,
				     GtkAdjustment *vadj);
void       kt_trackedit_set_point       (KTTrackEdit       *text,
				     guint          index);
guint      kt_trackedit_get_point       (KTTrackEdit       *text);
guint      kt_trackedit_get_length      (KTTrackEdit       *text);
void       kt_trackedit_freeze          (KTTrackEdit       *text);
void       kt_trackedit_thaw            (KTTrackEdit       *text);
void       kt_trackedit_insert          (KTTrackEdit       *text,
				     GdkFont       *font,
				     GdkColor      *fore,
				     GdkColor      *back,
				     const char    *chars,
				     gint           length);
gint       kt_trackedit_backward_delete (KTTrackEdit       *text,
				     guint          nchars);
gint       kt_trackedit_forward_delete  (KTTrackEdit       *text,
				     guint          nchars);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __KTTRACKEDIT_H__ */
