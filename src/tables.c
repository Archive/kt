/* 

Keg Tracker.  A GTK-based mod-editor a la fast-tracker.
Copyright (C) 1998 Pete Ryland

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author can be contacted at pdr@pdr.ml.org

*/

static char *_id = "$Id$";

#include <stdio.h>
#include "tables.h"
#include "diskop.h"
#include "stars.h"
#include "rowdisplay.h"

void nb_np_callback(GtkWidget *widget, gpointer data)
{
  gtk_notebook_next_page(GTK_NOTEBOOK(*(GtkWidget**)data));
}

void default_callback(GtkWidget *widget, gpointer data)
{
  fprintf(stderr, "%s\n", (char *)data);
}

extern GtkWidget *window;

void play_mode_callback(GtkWidget *widget, gpointer data) {
  fprintf(stderr, "bs\n");
  gtk_widget_set_usize(GTK_WIDGET(window), 700, 400);
  rowdisplay_hide();
}

void player_command(GtkWidget *widget, gpointer data);

GtkWidget *tlnb, *mnb;

KtStars stars_display;

void about_callback(GtkWidget *widget, gpointer data)
{
  kt_stars_start(stars_display.w, 700, 200);
  gtk_notebook_next_page(GTK_NOTEBOOK(mnb));
}

void stars_click_callback(GtkWidget *widget, gpointer data)
{
  gtk_notebook_next_page(GTK_NOTEBOOK(mnb));
  kt_stars_stop(stars_display.w);
}

ScopeDookaDo scopes[] = {
  {Scope, 0, 4, 6, 8, "Channel 1", 0, 0},
  {Scope, 4, 8, 6, 8, "Channel 2", 0, 1},
  {Scope, 8, 12, 6, 8, "Channel 3", 0, 2},
  {Scope, 12, 16, 6, 8, "Channel 4", 0, 3},
  {Scope, 0, 4, 8, 10, "Channel 5", 0, 4},
  {Scope, 4, 8, 8, 10, "Channel 6", 0, 5},
  {Scope, 8, 12, 8, 10, "Channel 7", 0, 6},
  {Scope, 12, 16, 8, 10, "Channel 8", 0, 7}
};

GtkButtonThing top_middle_buttons[] = {
  {RegButton, 24, 28, 0, 1, "About", 0, about_callback, "About", "About"},
  {RegButtonG, 24, 28, 1, 2, "Nibbles", 0, default_callback, "Nibbles", "Nibbles"},
  {RegButton, 24, 28, 2, 3, "Files", 0, nb_np_callback, &tlnb, "Files"},
  {RegButtonG, 24, 28, 3, 4, "Instr Ed", 0, default_callback, "Instr Ed", "Instr Ed"},
  {RegButtonG, 24, 28, 4, 5, "Sample Ed", 0, default_callback, "Sample Ed", "Sample Ed"},
  {RegButtonG, 24, 28, 5, 6, "CD Rip", 0, default_callback, "CD Rip", "CD Rip"},
  {RegButtonG, 24, 28, 6, 7, "Extended", 0, default_callback, "Extended", "Extended"},
  /*{RegButtonG, 24, 28, 7, 8, "Help", 0, default_callback, "Help", "Help"},*/
  {RegButton, 24, 28, 7, 8, "Player", 0, play_mode_callback, "Player", "Player"},
  {RegButton, 24, 28, 8, 9, "Config", 0, default_callback, "Config", "Config"},
  {RegButton, 24, 28, 9, 10, "Exit", 0, quit_callback, "Exit", "Exit"}
/*
  {RegButtonG, 20, 24, 2, 3, 0, 0, default_callback, "Zap", "Zap"},
  {RegButtonG, 20, 24, 3, 4, 0, 0, default_callback, "Zap All", "Zap All"},
  {RegButtonG, 20, 24, 4, 5, 0, 0, default_callback, "Extend", "Extend"},
  {RegButtonG, 20, 24, 5, 6, 0, 0, default_callback, "Transpose", "Transpose"},
  {RegButtonG, 20, 24, 6, 7, 0, 0, default_callback, "IE Ext", "IE Ext"},
  {RegButtonG, 20, 24, 7, 8, 0, 0, default_callback, "SE Ext", "SE Ext"},
  {RegButtonG, 20, 24, 8, 9, 0, 0, default_callback, "Add Chan", "Add Chan"},
  {RegButtonG, 20, 24, 9, 10, 0, 0, default_callback, "Sub Chan", "Sub Chan"},
  {RegButton, 24, 28, 0, 1, 0, 0, playsng_callback, "Play Song", "Play Song"},
  {RegButton, 24, 28, 1, 2, 0, 0, playpat_callback, "Play Pat", "Play Pat"},
  {RegButton, 24, 28, 2, 3, 0, 0, stopsng_callback, "Stop", "Stop"},
  {RegButtonG, 24, 28, 3, 4, 0, 0, default_callback, "Continue", "Continue"},
  {RegButtonG, 24, 28, 4, 5, 0, 0, default_callback, "Blum", "Blum"},
*/
};

GtkButtonThing top_left_normal_buttons[] = {
  {RegButtonG, 6, 8, 0, 1, "Ins", GTK_SHADOW_NONE, default_callback, "Ins", "Ins"},
  {RegButtonG, 6, 8, 2, 3, "Del", GTK_SHADOW_NONE, default_callback, "Del", "Del"},

  {RegButtonG, 18, 19, 4, 5, "*", GTK_SHADOW_NONE, default_callback, "*", "*"},
  {RegButtonG, 19, 20, 4, 5, "/", GTK_SHADOW_NONE, default_callback, "/", "/"}
};

GtkAdjustment *song_length_a, *rep_start_a, *curr_pat_a, *num_rows_a, *add_a, *tempo_a, *bpm_a;

SpinButtonDoodit top_left_normal_spin_buttons[] = {
  {SpinButton, 12, 16, 0, 1, "Song Length", GTK_SHADOW_NONE, 1, 256, 1, &song_length_a}, /* Song Length */
  {SpinButton, 12, 16, 1, 2, "Repeat Start", GTK_SHADOW_NONE, 0, 255, 1, &rep_start_a}, /* Repeat Start */
  {SpinButton, 12, 16, 2, 3, "Current Pattern", GTK_SHADOW_NONE, 0, 255, 1, &curr_pat_a}, /* Patterns */
  {SpinButton, 9, 11, 8, 9, "Number of Rows", GTK_SHADOW_NONE, 1, 256, 2, &num_rows_a}, /* Rows */
  {SpinButton, 13, 15, 8, 9, "Add", GTK_SHADOW_NONE, 1, 16, 1, &add_a} /* Add */
};

SliderBabe top_left_normal_sliders[] = {
  {Slider, 26, 29, 1, 5, "Speed", GTK_SHADOW_NONE, 1, 16, &tempo_a}, /* Speed */
  {Slider, 29, 32, 1, 10, "Beats Per Minute", GTK_SHADOW_NONE, 1, 241, &bpm_a} /* BPM */
};

/*
GtkButtonThing top_right_bank_buttons[] = {
  {RegButton, 36, 40, 0, 1, 0, 0, default_callback, "01-08", "01-08"},
  {RegButton, 36, 40, 1, 2, 0, 0, default_callback, "09-10", "09-10"},
  {RegButton, 36, 40, 2, 3, 0, 0, default_callback, "11-18", "11-18"},
  {RegButton, 36, 40, 3, 4, 0, 0, default_callback, "19-20", "19-20"},
  {RegButton, 36, 40, 4, 5, 0, 0, default_callback, "21-28", "21-28"},
  {RegButton, 36, 40, 5, 6, 0, 0, default_callback, "29-30", "29-30"},
  {RegButton, 36, 40, 6, 7, 0, 0, default_callback, "31-38", "31-38"},
  {RegButton, 36, 40, 7, 8, 0, 0, default_callback, "39-40", "39-40"},

  {RegButton, 36, 40, 8, 10, 0, 0, default_callback, "Swap Bank", "Swap Bank"}
};
*/

GtkPixmapButtonThingo top_left_pixmap_buttons[] = {
  {PixButtonG, 8, 16, 0, 2, 0, GTK_SHADOW_NONE, default_callback, "Keg Tracker", &(pixmaps[0])},
  {PixButtonG, 16, 20, 0, 2, 0, GTK_SHADOW_NONE, default_callback, "OATHSTIX!", &(pixmaps[1])},
  {PixButton, 4, 6, 0, 1, 0, GTK_SHADOW_NONE, player_command, (gpointer)'b', &(pixmaps[2])},
  {PixButton, 4, 6, 2, 3, 0, GTK_SHADOW_NONE, player_command, (gpointer)'f', &(pixmaps[3])},
  {PixButtonG, 6, 7, 1, 2, 0, GTK_SHADOW_NONE, default_callback, "Up", &(pixmaps[2])},
  {PixButtonG, 7, 8, 1, 2, 0, GTK_SHADOW_NONE, default_callback, "Down", &(pixmaps[3])}
/*
  {PixButton, 6, 7, 3, 4, 0, 0, default_callback, "Up", &(pixmaps[2])},
  {PixButton, 7, 8, 3, 4, 0, 0, default_callback, "Down", &(pixmaps[3])},
  {PixButton, 6, 7, 4, 5, 0, 0, default_callback, "Up", &(pixmaps[2])},
  {PixButton, 7, 8, 4, 5, 0, 0, default_callback, "Down", &(pixmaps[3])}
*/
};

GtkPixmapButtonThingo toolbar_pixmap_buttons[] = {
  {PixToggleButton, 0, 1, 8, 9, "Toggle Edit Mode", GTK_SHADOW_NONE, default_callback, "Rec", &(pixmaps[5])},
  {PixButton, 1, 2, 8, 9, "Play", GTK_SHADOW_NONE, playsng_callback, "Play", &(pixmaps[6])},
  {PixButton, 2, 3, 8, 9, "Play Pattern", GTK_SHADOW_NONE, playpat_callback, "Play Pattern", &(pixmaps[7])},
  {PixButton, 3, 4, 8, 9, "Stop", GTK_SHADOW_NONE, stopsng_callback, "Stop", &(pixmaps[8])},
  {PixButton, 4, 5, 8, 9, "Skip Back a Pattern in Order", GTK_SHADOW_NONE, player_command, (gpointer)'b', &(pixmaps[9])},
  {PixButton, 5, 6, 8, 9, "Skip Forward a Pattern in Order", GTK_SHADOW_NONE, player_command, (gpointer)'f', &(pixmaps[10])}
};

GtkButtonThing toolbar_buttons[] = {
  {CheckButton, 11, 13, 8, 9, "Add", GTK_SHADOW_NONE, default_callback, "Add", "Add"}
};

GtkPixmapThingo keg_piccie = {
  Pix, 0, 18, 5, 10, "Some Kegs", GTK_SHADOW_NONE, &(pixmaps[4])
};

GtkLabelWhatsAMe top_left_labels[] = {
  {Label, 8, 12, 0, 1, 0, 0, "Length"},
  {Label, 8, 12, 1, 2, 0, 0, "RepStrt"},
  {Label, 8, 12, 2, 3, 0, 0, "Pattern"},
  {Label, 26, 29, 0, 1, 0, 0, "Spd"},
  {Label, 29, 32, 0, 1, 0, 0, "BPM"},
  {Label, 7, 9, 8, 9, 0, 0, "Rows"}
};

GenericTableElem patdisplay = {
  PatDisplay, 0, 8, 0, 6, "Pattern Order", GTK_SHADOW_ETCHED_OUT
};

GenericTableElem *top_left_normal_table_elems[] = {
/*
  (GenericTableElem*)&top_left_normal_buttons[0],
  (GenericTableElem*)&top_left_normal_buttons[1],
  (GenericTableElem*)&top_left_normal_buttons[2],
  (GenericTableElem*)&top_left_normal_buttons[3],
  (GenericTableElem*)&top_left_pixmap_buttons[0],
  (GenericTableElem*)&top_left_pixmap_buttons[1],
  (GenericTableElem*)&top_left_pixmap_buttons[2],
  (GenericTableElem*)&top_left_pixmap_buttons[3], 
  (GenericTableElem*)&top_left_pixmap_buttons[4],
  (GenericTableElem*)&top_left_pixmap_buttons[5],
  (GenericTableElem*)&top_left_pixmap_buttons[6],
  (GenericTableElem*)&top_left_pixmap_buttons[7], 
  (GenericTableElem*)&top_left_pixmap_buttons[8],
  (GenericTableElem*)&top_left_pixmap_buttons[9],
*/
  (GenericTableElem*)&top_left_normal_spin_buttons[0],
  (GenericTableElem*)&top_left_normal_spin_buttons[1],
  (GenericTableElem*)&top_left_normal_spin_buttons[2],
  (GenericTableElem*)&top_left_normal_sliders[0],
  (GenericTableElem*)&top_left_normal_sliders[1],
/*
  (GenericTableElem*)&keg_piccie,
*/
  (GenericTableElem*)&top_left_labels[0],
  (GenericTableElem*)&top_left_labels[1],
  (GenericTableElem*)&top_left_labels[2],
  (GenericTableElem*)&top_left_labels[3],
  (GenericTableElem*)&top_left_labels[4],
  &patdisplay,
  (GenericTableElem*)&scopes[0],
  (GenericTableElem*)&scopes[1],
  (GenericTableElem*)&scopes[2],
  (GenericTableElem*)&scopes[3],
  (GenericTableElem*)&scopes[4],
  (GenericTableElem*)&scopes[5],
  (GenericTableElem*)&scopes[6],
  (GenericTableElem*)&scopes[7],
  0
};

TableStuff top_left_normal_ts = {
  10, 32, TRUE, top_left_normal_table_elems
};

TableStuff *top_left_ts[] = {
  &top_left_normal_ts,
  &top_left_diskop_ts,
  0
};

/*
GenericTableElem *toolbar_table_elems[] = {
  (GenericTableElem*)&toolbar_pixmap_buttons[0],
  0
}
*/

GtkNotepadThing top_left_np = {
  NPThing, 0, 24, 0, 10, 0, GTK_SHADOW_ETCHED_OUT, (TableStuff**)top_left_ts, &tlnb
};

GenericTableElem rowdisplay = {
  RowDisplay, 0, 25, 9, 20, 0, GTK_SHADOW_ETCHED_OUT
};

GenericTableElem instrdisplay = {
  InstrDisplay, 28, 40, 0, 6, "Instruments", GTK_SHADOW_ETCHED_OUT
};

GenericTableElem sampledisplay = {
  SampleDisplay, 28, 40, 6, 9, "Samples for Selected Instrument", GTK_SHADOW_ETCHED_OUT
};

GenericTableElem songnamedisplay = {
  SongName, 28, 40, 9, 10, "Song Title", GTK_SHADOW_NONE
};

GenericTableElem *main_table_elems[] = {
  (GenericTableElem*)&top_left_np,
  (GenericTableElem*)&top_middle_buttons[0],
  (GenericTableElem*)&top_middle_buttons[1],
  (GenericTableElem*)&top_middle_buttons[2],
  (GenericTableElem*)&top_middle_buttons[3],
  (GenericTableElem*)&top_middle_buttons[4],
  (GenericTableElem*)&top_middle_buttons[5],
  (GenericTableElem*)&top_middle_buttons[6],
  (GenericTableElem*)&top_middle_buttons[7],
  (GenericTableElem*)&top_middle_buttons[8],
  (GenericTableElem*)&top_middle_buttons[9],
/*
  (GenericTableElem*)&top_middle_buttons[10],
  (GenericTableElem*)&top_middle_buttons[11],
  (GenericTableElem*)&top_middle_buttons[12],
  (GenericTableElem*)&top_middle_buttons[13],
  (GenericTableElem*)&top_middle_buttons[14],
  (GenericTableElem*)&top_middle_buttons[15],
  (GenericTableElem*)&top_middle_buttons[16],
  (GenericTableElem*)&top_middle_buttons[17],
  (GenericTableElem*)&top_middle_buttons[18],
  (GenericTableElem*)&top_middle_buttons[19],
  (GenericTableElem*)&top_middle_buttons[20],
  (GenericTableElem*)&top_right_bank_buttons[0],
  (GenericTableElem*)&top_right_bank_buttons[1],
  (GenericTableElem*)&top_right_bank_buttons[2],
  (GenericTableElem*)&top_right_bank_buttons[3],
  (GenericTableElem*)&top_right_bank_buttons[4],
  (GenericTableElem*)&top_right_bank_buttons[5],
  (GenericTableElem*)&top_right_bank_buttons[6],
  (GenericTableElem*)&top_right_bank_buttons[7],
  (GenericTableElem*)&top_right_bank_buttons[8],
  (GenericTableElem*)&top_right_bank_buttons[9],
*/
  &instrdisplay,
  &sampledisplay,
  &songnamedisplay,
  0
};

TableStuff normal_main_ts = {
  10, 40, TRUE, main_table_elems
};

KtStars stars_display = {
  Stars, 0, 1, 0, 1, "Starfield by Rasterman\nLogo by Tigert", GTK_SHADOW_NONE,
  stars_click_callback, "Stars", &(pmaps[0]), 0
};

GenericTableElem *stars_table_elems[] = {
  (GenericTableElem*)&stars_display,
  0
};

TableStuff stars_main_ts = {
  1, 1, TRUE, stars_table_elems
};

TableStuff *main_ts[] = {
  &normal_main_ts,
  &stars_main_ts,
  0
};

GtkNotepadThing main_np = {
  NPThing, 0, 25, 0, 8, 0, GTK_SHADOW_NONE, (TableStuff**)main_ts, &mnb
};

GenericTableElem *table_elems[] = {
  (GenericTableElem*)&main_np,
  &rowdisplay,
  (GenericTableElem*)&toolbar_pixmap_buttons[0],
  (GenericTableElem*)&toolbar_pixmap_buttons[1],
  (GenericTableElem*)&toolbar_pixmap_buttons[2],
  (GenericTableElem*)&toolbar_pixmap_buttons[3],
  (GenericTableElem*)&toolbar_pixmap_buttons[4],
  (GenericTableElem*)&toolbar_pixmap_buttons[5],
  (GenericTableElem*)&top_left_labels[5], /* Rows */
  (GenericTableElem*)&top_left_normal_spin_buttons[3], /* Rows */
  (GenericTableElem*)&toolbar_buttons[0], /* Add */
  (GenericTableElem*)&top_left_normal_spin_buttons[4], /* Add */
  0
};

TableStuff ts = {
  20, 25, TRUE, table_elems
};
