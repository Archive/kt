/*

Keg Tracker.  A GTK-based mod-editor a la fast-tracker.
Copyright (C) 1998 Pete Ryland

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author can be contacted at pdr@pdr.ml.org

*/

static char *_id = "$Id$";

#include <gtk/gtk.h>
#include <stdio.h>

#include "KandW.xpm"
#include "kt.xpm"
#include "up.xpm"
#include "down.xpm"
#include "pix.h"
#include "logo.xpm"
#include "rec.xpm"
#include "play.xpm"
#include "play2.xpm"
#include "stop.xpm"
#include "rew.xpm"
#include "ff.xpm"

PixmapThingy pixmaps[] = {
  {kt_xpm, 0, 0}, {KandW_xpm, 0, 0},
  {up_xpm, 0, 0}, {down_xpm, 0, 0},
  {logo_xpm, 0, 0}, {rec_xpm, 0, 0},
  {play_xpm, 0, 0}, {play2_xpm, 0, 0},
  {stop_xpm, 0, 0}, {rew_xpm, 0, 0},
  {ff_xpm, 0, 0}
};
int npixmaps = sizeof(pixmaps) / sizeof(pixmaps[0]);

PixmapThingy pmaps[] = {
  {logo_xpm, 0, 0}
};
int npmaps = sizeof(pmaps) / sizeof(pmaps[0]);

void setup_pixmaps(GtkWidget *window) {
  int i;
  PixmapThingy *p = pixmaps;
  GtkStyle *style;

  style = gtk_widget_get_style(window);
  for (i = npixmaps; i--; p++) {
    p->pixmap = gdk_pixmap_create_from_xpm_d(window->window, &(p->mask),
                   &style->bg[GTK_STATE_NORMAL], (gchar **)p->xpm_data);
    if (p->pixmap == 0) {
      fprintf(stderr, "Blah, the pixmap doesn't load\n");
      exit(1);
    }
  }
  p = pmaps;
  for (i = npmaps; i--; p++) {
    p->pixmap = gdk_pixmap_colormap_create_from_xpm_d(window->window,
                   gtk_widget_get_colormap(window), &(p->mask),
                   &style->bg[GTK_STATE_NORMAL], (gchar **)p->xpm_data);
    if (p->pixmap == 0) {
      fprintf(stderr, "Blah, the pixmap doesn't load\n");
      exit(1);
    }
  }
}
