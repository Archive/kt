# Note that this is NOT a relocatable package
%define ver      0.4.4
%define rel      1
%define prefix   /usr/local

Summary: KegTracker
Name: kt
Version: %ver
Release: %rel
Copyright: GPL
Group: X11/Applications/Sound
Source: ftp://pdr.ml.org/pub/kt/kt-%{ver}.tar.gz
BuildRoot: /tmp/kt-root
Packager: Widget <pdr@pdr.ml.org>
URL: http://pdr.ml.org
Requires: glib >= 1.1.0
Requires: gtk+ >= 1.1.0
Requires: xmp-kt = %{ver}

%description
A GPLed GTK-based mod music editor, based on xmp by Claudio Matsuoka.
KegTracker is written by Widget <pdr@pdr.ml.org> and
K <conradp@cse.unsw.edu.au>.

%prep
%setup

%build
CFLAGS="${RPM_OPT_FLAGS}"
./autogen.sh --prefix=%prefix
make

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%{prefix}/bin/kt
%doc doc/AUTHORS doc/COPYING doc/INSTALL doc/ChangeLog doc/NEWS doc/README

%changelog

* Sun Nov 15 1998 Widget <pdr@pdr.ml.org>

- wrote .spec file
